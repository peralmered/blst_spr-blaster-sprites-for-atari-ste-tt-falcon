;//////////////////////////////////////////////////////////////////////////////////////////////
;// Mallory: Atari ST/STE memory manager


; Calls
;   mallory_init  ; the actual one-time allocation of memory
;     ; In:  d0.l - Amount of RAM requested
;     ; Out: d0.w - 0 if ok, otherwise error code
;   
;   mallory_gimme
;     ; In:  d0.l - Amount of RAM requested
;     ;      d1.w - 0 = no align, 1=word align, 2=long align
;     ; Out: d0.w - 0 if ok, otherwise error code
;     ;      d1.l - start of requested memory
;   
;   mallory_clear_all  ; actually only resets pointer, clears nothing
;   
;   mallory_exit  ; does jack shit, but makes it all look very very professional


; Defines
val set 1
MALLORY_ERROR_NOT_ENOUGH_MEMORY equ val  ; failed getting (enough) memory
val set val+1
MALLORY_ERROR_MALLORY_ALREADY_ENGAGED equ val  ; mallory already inited
val set val+1


mallory_init:
; In:  d0.l - Amount of RAM requested
; Out: d0.w - 0 if ok, otherwise error code

  tst.w mallory_inited
  beq .go

.mallory_already_engaged:
  move.w #MALLORY_ERROR_MALLORY_ALREADY_ENGAGED,d0
  rts

.go:
  move.w #1,mallory_inited
  move.l d0,d6
  ; malloc()  ; returns error in d0
  move.l d0,-(sp)
  move.w #$48,-(sp)
  trap #1
  add.l #6,sp
  tst.w d0
  bne .error
  move.l d0,d7
  move.l d7,mallory_start
  move.l d7,mallory_here
  move.l d6,mallory_size
  add.l d6,d7
  move.l d7,mallory_end
  rts

.error
  move.w #MALLORY_ERROR_NOT_ENOUGH_MEMORY,d0
  rts


;------------------------------------------------------------------------------


mallory_gimme:
; In:  d0.l - Amount of RAM requested
;      d1.w - 0 = no align, 1=word align, 2=long align
; Out: d0.w - 0 if ok, otherwise error code
;      d1.l - start of requested memory
  move.l mallory_here,d7

    ; Any align at all?
    tst.w d1
    beq.s .align_done
    ; Word align?
    btst #0,d1
    beq.s .no_word_align
    addq.l #1,d7
    bclr #0,d7
    bra.s .align_done
  .no_word_align:
    ; Long align?
    btst #1,d1
    beq.s .no_long_align
    addq.l #3,d7
    bclr #0,d7
    bclr #1,d7
    bra.s .align_done
  .no_long_align:
.align_done:

  move.l d7,d6
  add.l d0,d6
  move.l mallory_end,d5
  cmp.l d5,d6
  bgt .error

  ; All's well
  move.l d7,d1
  add.l d0,d7
  move.l d7,mallory_here
  clr.w d0
  rts

.error
  move.w #MALLORY_ERROR_NOT_ENOUGH_MEMORY,d0
  rts


;------------------------------------------------------------------------------


mallory_clear_all:
  move.l mallory_start,mallory_here
  rts


;------------------------------------------------------------------------------


mallory_exit:
  ; release memory, except we don't, so meh. Just return.
  rts


;------------------------------------------------------------------------------

  .bss
mallory_inited: ds.w 1  ; flag for whether mallory is already active
mallory_start:  ds.l 1  ; position in RAM for start of block
mallory_size:   ds.l 1  ; number of bytes free from mallory_start
mallory_end:    ds.l 1  ; position in RAM for end of block (i e the byte AFTER the last free one)
mallory_here:   ds.l 1  ; "head" in malloc:ed memory area
  .68000


;// Mallory: Atari ST/STE memory manager
;//////////////////////////////////////////////////////////////////////////////////////////////
