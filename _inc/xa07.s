; xa STE/TT/Falcon code

; For more information, see xa.txt. Or not. I'm not your mother. Take some responsibility yourself.

;##############################################################################################
;## xa07

; Todo
; =====
; # VFT: Search for "ToDo" to read
; # add documentation for set_params
; # Optimze PFT innerloops to remove adding to A2; instead, add before or after loop
; # Optimize RAW mode
;
;
; Done
; =====
;
;
;

;--------------------------------------------------------------
;-- File format
;
;--[ Header ]---------------------------------
;offset 00 - 03  [4 bytes] "xa07" text, 4 bytes
;offset 04       [1 byte] Number of bitplanes in image data
;offset 05       [1 byte] Palette size as 2^x: 4=16 colors, 8=256 colors, 0=truecolor (unused)
;offset 06       [1 byte] Color format: 0=ST/STe, 1=TT, 2=Falcon 6-bit
;offset 07       [1 byte] Looping animation? 0=no, 1=yes
;offset 08- 09   [1 word] Offset to directory from absolute 00 of file
;offset 10-      [0, 16, 256 or 512 words] (Palette: true color, 4bpl, 8bpl TT and 8bpl Falcon respectively)
;
;--[ Directory ]------------------------------
;offset X - X+1    [1 word] Number of frames (0-65535)
;offset X+2 - X+3  For each frame:
;                    [1 longword] offset from absolute 00 of file
;
;--[ Frame data ]-----------------------------
;offset 00 - 01 [1 word] block id
;offset 02 -    block data

;--[ RAW format ]-----------------------------
; offset 00 - 03 [1 long] number of bytes
; offset 04 -    [bytes] data

;--[ GGN PLUS format ]------------------------
; offset 00 - 01 [1 word] number of 64K blocks
;   64K block:
;     offset 00 - 01 [1 word] number of chunks in 64K block
;       Chunk:
;         offset 00 - 01 [1 word] chunk id
;           Chunk Id XA_CMD_GGN_PLUS_AREA_END:
;             [no data]
;           Chunk Id XA_CMD_GGN_PLUS_CLEAR_LIST:
;             offset 00 - 01 [1 word] number of clear offsets
;               Clear offset:
;                 offset 00 - 01 [1 word] clear offset
;           Chunk Id XA_CMD_GGN_PLUS_BACKGROUND_LIST:
;             offset 00 - 01 [1 word] number of background offsets
;               Background offset:
;                 offset 00 - 01 [1 word] background offset
;           Chunk Id XA_CMD_GGN_PLUS_DATA_LIST:
;             offset 00 - 01 [1 word] number of data blocks
;               Data block:
;                 offset 00 - 01 [1 word] data offset
;                 offset 02 - 03 [1 word] data

;--[ PFT format ]-----------------------------
; PFT data is a series of word-sized commands, which are repeated until the occurrence of the
; XA_CMD_PFT_END command, at which control is returned to the calling code.
; Commands are given either in short or long forms. In short forms, the "length" element is in
; the second byte of the command word. In short form, the "length" element is in the word
; following the command.
;
; Commands:
;   XA_CMD_PFT_SKIP        ($F0nn or $F000, nnnn)
;     Skip nn bitplane chunks in dest
;   XA_CMD_PFT_DATA        ($F1nn or $F100, nnnn)
;     After command follows nn chunks of data
;   XA_CMD_PFT_CLEAR       ($F2nn or $F200, nnnn)
;     Clear nn chunks in dest
;   XA_CMD_PFT_BACKGROUND  ($F3nn or $F300, nnnn)
;     Copy nn chunks from background buffer to dest
;   XA_CMD_PFT_END         ($FFnn or $FF00, nnnn)
;     End of frame



;###############################################################################################
;## Optimizations

if CPU_MODEL_68030
  OPT_JUMP_INTO_CODE_LIST equ 0
;OPT_RAW_MODE_CODE_LIST equ 1
endif

if CPU_MODEL_68000
  OPT_JUMP_INTO_CODE_LIST equ 1 ; seems unlikely this IS actually faster on 68000
;OPT_RAW_MODE_CODE_LIST equ 1
endif

OPT_RAW_MODE_CODE_LIST equ 0

;## Optimizations
;###############################################################################################


XA_FRAME_RAW_MODE       equ $BEEF
XA_FRAME_GGN_MODE       equ $6600
XA_FRAME_GGN_PLUS_MODE  equ $660D
XA_FRAME_DEFJAM_MODE    equ $DEFF
XA_FRAME_PFT_MODE       equ $DF70
XA_FRAME_VFT_MODE       equ $F457

XA_CMD_GGN_PLUS_CLEAR_LIST        equ $FADE
XA_CMD_GGN_PLUS_AREA_END          equ $EDED
XA_CMD_GGN_PLUS_BACKGROUND_LIST   equ $BACC
XA_CMD_GGN_PLUS_DATA_LIST         equ $DA7A

XA_CMD_PFT_SKIP         equ $F000 ; actually $F0nn, where nn is number of chunks, nn=0 means read next word as nn
XA_CMD_PFT_DATA         equ $F100
XA_CMD_PFT_CLEAR        equ $F200
XA_CMD_PFT_BACKGROUND   equ $F300
XA_CMD_PFT_END          equ $FF00

COLOR_FORMAT_NUMERIC_STE      equ 0
COLOR_FORMAT_NUMERIC_TT       equ 1
COLOR_FORMAT_NUMERIC_FALCON   equ 2

XA_CMD_VFT_END_OF_FRAME                 equ 0<<13
XA_CMD_VFT_END_OF_64K_BLOCK             equ 1<<13
XA_CMD_VFT_CLEAR_LIST                   equ 2<<13
XA_CMD_VFT_BACKGROUND_LIST              equ 3<<13
XA_CMD_VFT_IMM_DATA_LONGWORD_LIST       equ 4<<13
XA_CMD_VFT_IMM_DATA_WORD_LIST           equ 5<<13
XA_CMD_VFT_REG_DATA_LONGWORD_LIST       equ 6<<13
XA_CMD_VFT_REG_DATA_WORD_LIST           equ 7<<13

XA_VFT_COMMAND_MASK  equ %1110000000000000
XA_VFT_COUNTER_MASK  equ %0001111111111111


;##
;##############################################################################################

xa_image_bitplanes:      dc.w 0
xa_screen_bitplanes:     dc.w 0
xa_image_words:          dc.w 0
xa_screen_words_adder:   dc.w 0
xa_shift_for_skip:       dc.w 0

;###############################################################

xa_set_params::
; In: d0.l - number of bitplanes in screen mode
;     a0.l - pointer to XA file

  move.w d0,xa_screen_bitplanes
  cmp.b #8,d0
  beq .xa_set_params_8bpl
  cmp.b #4,d0
  beq .xa_set_params_4bpl
  cmp.b #2,d0
  beq .xa_set_params_2bpl
  bra .xa_set_params_1bpl

.xa_set_params_8bpl:
  move.w #4,xa_shift_for_skip
  bra .xa_set_params_bitplanes_done

.xa_set_params_4bpl:
  move.w #3,xa_shift_for_skip
  bra .xa_set_params_bitplanes_done

.xa_set_params_2bpl:
  move.w #2,xa_shift_for_skip
  bra .xa_set_params_bitplanes_done

.xa_set_params_1bpl:
  move.w #1,xa_shift_for_skip
  ;bra .xa_set_params_bitplanes_done

.xa_set_params_bitplanes_done:
  cmp.l #"xa07",(a0)
  bne .xa_set_params_exit
  move.l #0,d1
  move.b 4(a0),d1
  move.w d1,xa_image_bitplanes
  move.w d1,xa_image_words
  sub.w d1,d0
  lsl.w #1,d0
  move.w d0,xa_screen_words_adder

.xa_set_params_exit:
  rts

;###############################################################

xa_get_number_of_frames::
; In:  a0.l - xa07 data
; Out: d0.l - number of frames

  move.w 8(a0),d0 ; get offset to directory
  move.w (a0,d0),d0 ; get number of frames
  ext.l d0
  sub.l #1,d0
  rts

;###############################################################

xa_get_palette::
; In:  a0.l - xa07 data
;      a1.l - palette destination
; Out: writes XA file's palette to (a1)
  pushall
  move.b 5(a0),d0 ; palette size
  move.b 6(a0),d1 ; color format
  add.l #10,a0
  cmp.b #4,d0
  beq .palette16

  cmp.b #COLOR_FORMAT_NUMERIC_FALCON,d1
  bne .palette256_word
  
.palette256_long:
  move.l #256-1,d7
  bra .copy
.palette256_word:
  move.l #256/2-1,d7
.copy:
  move.l (a0)+,(a1)+
  dbra d7,.copy
  bra .done

.palette16: ; 68000 code, but so small it won't matter on 68030
  movem.l (a0),d0-d7
  movem.l d0-d7,(a1)

.done:
  popall
  rts

;###############################################################

xa_unpack_one_frame::
; In: a0.l - xa07 data
;     a1.l - screen address
;     a2.l - background screen

  cmp.l #"xa07",(a0)
  bne .skipheader

  move.w 8(a0),d0 ; get offset to directory
  move.l 2(a0,d0),d0 ; get first offset from directory
  add.l d0,a0 ; a0 now points to first frame

.skipheader:
  move.w (a0)+,d0
  
  cmp.w #XA_FRAME_VFT_MODE,d0
  beq .xa_mode_vft

  cmp.w #XA_FRAME_GGN_MODE,d0
  beq .xa_mode_ggn
  
  cmp.w #XA_FRAME_PFT_MODE,d0
  beq .xa_mode_pft

  cmp.w #XA_FRAME_GGN_PLUS_MODE,d0
  beq .xa_mode_ggn_plus  

  cmp.w #XA_FRAME_RAW_MODE,d0
  beq .xa_mode_raw

  ;cmp.w #XA_FRAME_DEFJAM_MODE,d0
  ;beq .xa_mode_defjam
  
.unpack_frame_error:
  move.w #$700,$ffff8240
  move.w #$070,$ffff8240
  bra .unpack_frame_error
  
  rts  
  
;###############################################################

.xa_mode_ggn:
; In: a0.l - xa07 data
;     a1.l - screen address

  add.l #32768,a1 ; largest signed word-aligned offset                         ; _16_
  move.w (a0)+,d7 ; number of 64K areas                                        ;  _8_
  subq.w #1,d7                                                                 ;  _4_
.ggn_arealoop:

  if CPU_MODEL_68030 ; if cpu is 030
    move.w (a0)+,d6
.ggn_chunkloop:
      move.w (a0)+,d0
      move.w (a0)+,(a1,d0)
    dbra d6,.ggn_chunkloop
  endif

  if CPU_MODEL_68000 ; if cpu is 000
    move.w (a0)+,d6                                                            ;  _8_
    sub.w #1,d6                                                                ;  _4_
.ggn_chunkloop:
      move.w (a0)+,d0                                                          ;  _8_
      ext.l d0                                                                 ;  _4_
      move.w (a0)+,(a1,d0.l)                                                   ; _20_
    dbra d6,.ggn_chunkloop                                   ; (not taken: 16) ; _12_
  endif
   
    add.l #65536,a1                                                            ; _16_

  dbra d7,.ggn_arealoop                                      ; (not taken: 16) ; _12_
  rts

;###############################################################

.xa_mode_ggn_plus:  ; currently (xa07, 2017-09-17) not implemented
; In: a0.l - xa07 data
;     a1.l - screen address
;     a2.l - address to background unpacked

  move.l #0,d2
  add.l #32768,a1 ; largest signed word-aligned offset
  add.l #32768,a2
  move.w (a0)+,d7 ; number of 64K areas
  sub.l #1,d7
.ggn_plus_arealoop:

.ggn_plus_get_command:
  move.w (a0)+,d0

  cmp.w #XA_CMD_GGN_PLUS_DATA_LIST,d0
  beq .ggn_plus_data_list

  cmp.w #XA_CMD_GGN_PLUS_CLEAR_LIST,d0
  beq .ggn_plus_clear_list

  cmp.w #XA_CMD_GGN_PLUS_BACKGROUND_LIST,d0
  beq .ggn_plus_background_list

  cmp.w #XA_CMD_GGN_PLUS_AREA_END,d0
  beq .ggn_plus_next_area

  ; ERROR - no valid command
.xa_ggn_plus_bang: ; Code should never end up here
  move.w #$f00,$ffff8240
  move.w #$400,$ffff8240
  bra .xa_ggn_plus_bang


.ggn_plus_data_list:
  if 1=0
    move.l (a0)+,d6
    sub.l #1,d6
    swap d6
.ggn_plus_data_chunkloop_outer:
      swap d6
.ggn_plus_data_chunkloop_inner:
        move.w (a0)+,d0
        move.w (a0)+,(a1,d0.w)
      dbra d6,.ggn_plus_data_chunkloop_inner
      swap d6
    dbra d6,.ggn_plus_data_chunkloop_outer
    bra .ggn_plus_get_command
  endif

  if CPU_MODEL_68030 ; if cpu is 030
  move.w (a0)+,d6
  sub.w #1,d6
.ggn_plus_data_chunkloop:
    move.w (a0)+,d0
    move.w (a0)+,(a1,d0)
  dbra d6,.ggn_plus_data_chunkloop
  endif

  if CPU_MODEL_68000 ; if cpu is 000
  ; ***** This is 030 code, unroll for 000! *****
  move.w (a0)+,d6
  sub.w #1,d6
.ggn_plus_data_chunkloop:
    move.w (a0)+,d0
    move.w (a0)+,(a1,d0)
  dbra d6,.ggn_plus_data_chunkloop
  endif

  bra .ggn_plus_get_command


.ggn_plus_clear_list:
  if 1=0
    move.l (a0)+,d6
    sub.l #1,d6
    swap d6
.ggn_plus_clear_chunkloop_outer:
      swap d6
.ggn_plus_clear_chunkloop_inner:
        move.w (a0)+,d0
        move.w d2,(a1,d0)
      dbra d6,.ggn_plus_clear_chunkloop_inner
      swap d6
    dbra d6,.ggn_plus_clear_chunkloop_outer
    bra .ggn_plus_get_command
  endif


  if CPU_MODEL_68030 ; if cpu is 030
  move.w (a0)+,d6
  sub.w #1,d6
.ggn_plus_clear_chunkloop:
    move.w (a0)+,d0
    move.w d2,(a1,d0)
  dbra d6,.ggn_plus_clear_chunkloop
  endif

  if CPU_MODEL_68000 ; if cpu is 000
  ; ***** This is 030 code, unroll for 000! *****
  move.w (a0)+,d6
  sub.w #1,d6
.ggn_plus_clear_chunkloop:
    move.w (a0)+,d0
    move.w d2,(a1,d0)
  dbra d6,.ggn_plus_clear_chunkloop
  endif


  bra .ggn_plus_get_command


.ggn_plus_background_list:
  if 1=0
    move.l (a0)+,d6
    sub.l #1,d6
    swap d6
.ggn_plus_background_chunkloop_outer:
      swap d6
.ggn_plus_background_chunkloop_inner:
        move.w (a0)+,d0
        move.w (a2,d0),(a1,d0)
      dbra d6,.ggn_plus_background_chunkloop_inner
      swap d6
    dbra d6,.ggn_plus_background_chunkloop_outer
    bra .ggn_plus_get_command
  endif


  if CPU_MODEL_68030 ; if cpu is 030
  move.w (a0)+,d6
  sub.w #1,d6
.ggn_plus_background_chunkloop:
    move.w (a0)+,d0
    move.w (a2,d0),(a1,d0)
  dbra d6,.ggn_plus_background_chunkloop
  endif

  if CPU_MODEL_68000 ; if cpu is 000
  ; ***** This is 030 code, unroll for 000! *****
  move.w (a0)+,d6
  sub.w #1,d6
.ggn_plus_background_chunkloop:
    move.w (a0)+,d0
    move.w (a2,d0),(a1,d0)
  dbra d6,.ggn_plus_background_chunkloop
  endif

    bra .ggn_plus_get_command


.ggn_plus_next_area:
    add.l #65536,a1
    add.l #65536,a2

  dbra d7,.ggn_plus_arealoop
  rts

;###############################################################

.xa_mode_raw:
; In: a0.l - xa07 data
;     a1.l - screen address

  move.l (a0)+,d7                                                              ;  _8_
  ;lsr.l #2,d7
  subq.l #1,d7                                                                 ;  _8_
  swap d7                                                                      ;  _4_
.raw_outer_loop:
  swap d7                                                                      ;  _4_

  if OPT_RAW_MODE_CODE_LIST
  
  else
  
.raw_inner_loop:
  ;move.l (a0)+,(a1)+
  move.b (a0)+,(a1)+                                                           ; _12_
  dbra d7,.raw_inner_loop                                    ; (not taken: 16) ; _12_

  endif
 
  swap d7                                                                      ;  _4_
  dbra d7,.raw_outer_loop                                    ; (not taken: 16) ; _12_

  rts

;###############################################################

.xa_mode_pft:
; In: a0.l - xa07 data
;     a1.l - screen address

.xa_pft_get_block:
  move.w (a0),d0                                                               ;  _8_
  cmp.w #XA_CMD_PFT_END,d0                                                     ;  _8_
  bne.s .xa_pft_no_exit                                       ; (not taken: 8) ; _12_
  addq.l #2,a0                                                                 ;  _8_
  ;bra .xa_pft_exit            ; _12_
  rts
.xa_pft_no_exit:

  moveq.l #0,d7                                                                ;  _4_
  move.b 1(a0),d7                                                              ; _12_
  cmp.b #0,d7                                                                  ;  _8_
  bne.s .xa_pft_short_form                                    ; (not taken: 8) ; _12_
  bra.s .xa_pft_long_form                                                      ; _12_
.xa_pft_short_form:
  clr.b d0                                                                     ;  _4_
  bra .xa_pft_go_on                                                            ; _12_
.xa_pft_long_form:
  addq.l #2,a0                                                                 ;  _8_
  move.w (a0),d7                                                               ;  _8_
  ext.l d7                                                                     ;  _4_
  ; At this point, length is in d7
  
.xa_pft_go_on:
  add.w #2,a0                                                                  ;  _8_
  cmp.w #XA_CMD_PFT_SKIP,d0                                                    ;  _8_
  beq.s .xa_pft_skip                                          ; (not taken: 8) ; _12_
  cmp.w #XA_CMD_PFT_DATA,d0                                                    ;  _8_
  beq.s .xa_pft_data                                          ; (not taken: 8) ; _12_
  cmp.w #XA_CMD_PFT_CLEAR,d0                                                   ;  _8_
  beq .xa_pft_clear                                          ; (not taken: 12) ; _12_
  cmp.w #XA_CMD_PFT_BACKGROUND,d0                                              ;  _8_
  beq .xa_pft_background                                     ; (not taken: 12) ; _12_

.xa_pft_bang: ; Code should never end up here
  move.w #$f00,$ffff8240
  move.w #$400,$ffff8240
  bra .xa_pft_bang


.xa_pft_skip:
  move.w xa_shift_for_skip,d6                                                  ; _16_
  ext.l d6                                                                     ;  _4_
  lsl.w d6,d7                                                                  ; 8bpl=12c, 4bpl=12c, 2bpl=8c, 1bpl=8c
  add.l d7,a1                                                                  ;  _8_
  add.l d7,a2                                                                  ;  _8_
  bra .xa_pft_get_block                                                        ; _12_

.xa_pft_data:
  move.w xa_image_bitplanes,d5                                                 ; _16_
  move.w xa_screen_words_adder,d4                                              ; _16_
  ext.l d5                                                                     ;  _4_

  if OPT_JUMP_INTO_CODE_LIST

  ;subq.l #1,d5
  subq.l #1,d7                                                                 ;  _8_
.xa_pft_data_loop_outer:
  ;break
;    add.w #1,d5
.xa_pft_data_loop_inner_loopback:
    move.w d5,d6                                                               ;  _4_
    and.w #$ff,d6                                                              ;  _8_
    lea .xa_pft_data_loop_inner_codeloop_end,a6                                ; _12_
    add.w d6,d6                                                                ;  _4_
    add.w d6,d6                                                                ;  _4_
    sub.w d6,a6                                                                ;  _4_
    jmp (a6)                                                                   ;  _8_
.xa_pft_data_loop_inner_codeloop_start:
    rept 256
      move.w (a0)+,(a1)+                                                       ; _12_
      addq.w #2,a2                                                             ;  _4_
    endr
.xa_pft_data_loop_inner_codeloop_end:
    sub.w #256,d5                                                              ;  _8_
    bpl .xa_pft_data_loop_inner_loopback                     ; (not taken: 12) ; _12_

  else

    subq.l #1,d5                                                               ;  _8_
    subq.l #1,d7                                                               ;  _8_
.xa_pft_data_loop_outer:
    ;break
    move.l d5,d6                                                               ;  _8_
.xa_pft_data_loop_inner:
    move.w (a0)+,(a1)+ ; 2 bytes                                               ; _12_
    addq.w #2,a2 ; 2 bytes?                                                    ;  _4_
    dbra d6,.xa_pft_data_loop_inner                          ; (not taken: 16) ; _12_

  endif

  add.w d4,a1                                                                  ;  _8_
  dbra d7,.xa_pft_data_loop_outer                            ; (not taken: 16) ; _12_
  bra .xa_pft_get_block                                                        ; _12_

.xa_pft_clear:
  move.w xa_screen_bitplanes,d5                                                ; _16_
  move.w xa_screen_words_adder,d4                                              ; _16_
  move.l #0,d3                                                                 ; _12_
  ext.l d5
  
  if OPT_JUMP_INTO_CODE_LIST
  
    ;sub.l #1,d5
    subq.l #1,d7                                                               ;  _8_
.xa_pft_clear_loop_outer:
.xa_pft_clear_loop_inner_loopback:
    move.w d5,d6                                                               ;  _4_
    and.w #$ff,d6                                                              ;  _8_
    lea .xa_pft_clear_loop_inner_codeloop_end,a6                               ; _12_
    add.w d6,d6                                                                ;  _4_
    add.w d6,d6                                                                ;  _4_
    sub.w d6,a6                                                                ;  _4_
    jmp (a6)                                                                   ;  _8_
.xa_pft_clear_loop_inner_codeloop_start:
    rept 256
      move.w d3,(a1)+                                                          ;  _8_
      addq.w #2,a2                                                             ;  _4_
    endr
.xa_pft_clear_loop_inner_codeloop_end:
    sub.w #256,d5                                                              ;  _8_
    bpl .xa_pft_clear_loop_inner_loopback                    ; (not taken: 12) ; _12_

  else
  
    subq.l #1,d5                                                               ;  _8_
    subq.l #1,d7                                                               ;  _8_
.xa_pft_clear_loop_outer:
    move.l d5,d6                                                               ;  _8_
.xa_pft_clear_loop_inner:
    move.w d3,(a1)+                                                            ;  _8_
    addq.w #2,a2                                                               ;  _4_
    dbra d6,.xa_pft_clear_loop_inner                         ; (not taken: 16) ; _12_
  
  endif
  
  add.w d4,a1                                                                  ;  _8_
  dbra d7,.xa_pft_clear_loop_outer                           ; (not taken: 16) ; _12_
  bra .xa_pft_get_block                                                        ; _12_

.xa_pft_background:
  move.w xa_image_bitplanes,d5                                                 ; _16_
  move.w xa_screen_words_adder,d4                                              ; _16_
  ext.l d5                                                                     ;  _4_

  subq.l #1,d5                                                                 ;  _8_
  subq.l #1,d7                                                                 ;  _8_
.xa_pft_background_loop_outer:
  ;break
  move.l d5,d6                                                                 ;  _8_
.xa_pft_background_loop_inner:
  move.w (a2)+,(a1)+                                                           ; _12_
  dbra d6,.xa_pft_background_loop_inner                      ; (not taken: 16) ; _12_

  add.w d4,a1                                                                  ;  _8_
  add.w d4,a2                                                                  ;  _8_
  dbra d7,.xa_pft_background_loop_outer                      ; (not taken: 16) ; _12_
  bra .xa_pft_get_block                                                        ; _12_


.xa_pft_block_done:
  bra .xa_pft_get_block                                                        ; _12_


.xa_pft_exit:
  ;add.l #2,a0 ; go past END marker
  rts


;###############################################################

; ToDo
; # Extract inner loop and replace by "Unroll16" calls
;    macro Unroll16 mymacro, loopreg
;    ; Runs the "mymacro" macro "loopreg" (data register) times, unrolled in chunks of 16
;    ; Warning: loopreg must be 32767 or lower
;    ; Trashes: loopreg


.xa_mode_vft:
; In:  a0.l - pointer to VFT frame data
;      a1.l - pointer to frame buffer
;      a2.l - pointer to background frame buffer

  add.l #32768,a1                                                              ; _16_

  move.w #XA_VFT_COMMAND_MASK,d7                                               ;  _8_
  move.w #XA_VFT_COUNTER_MASK,d6                                               ;  _8_
  moveq #1,d5 ;/ for adding 64k to pointers (with line below)                  ;  _4_
  swap.w d5   ;\ faster than "move.l #65536,d5"                                ;  _4_

.get_vft_command:
  move.w (a0)+,d0                                                              ;  _8_
  move.w d0,d1                                                                 ;  _4_
  and.w d6,d1 ; d1.w = counter                                                 ;  _4_
  and.w d7,d0 ; d0.w = command                                                 ;  _4_
  lsl.l #5,d0 ; moves 3-bit command into top word AND shifts it up twice       ; _16_
  swap.w d0 ; d0 == 0<<2 -- 7<<2                                               ;  _4_
  lea .vft_command_jump_table,a6                                               ; _12_
  add.w d0,a6                                                                  ;  _8_
  move.l (a6),a6                                                               ; _12_
  jmp (a6)                                                                     ;  _8_
  bra fatal_error

;------------------------------------------------------------------------------
;-- end of frame

.cmd_vft_end_of_frame:
  rts

;-- end of frame
;------------------------------------------------------------------------------
;-- end of 64k block

.cmd_vft_end_of_64k_block:
  add.l d5,a1                                                                  ;  _8_
  add.l d5,a2                                                                  ;  _8_
  bra .get_vft_command                                                         ; _12_

;-- end of 64k block
;------------------------------------------------------------------------------
;-- clear list

.cmd_vft_clear_list:
;    swap d1
;    swap d1
  move.w (a0)+,d2 ; number of words to clear                                   ;  _8_
  moveq #0,d4                                                                  ;  _4_
  tst.w d1                                                                     ;  _4_
  beq .clear_list_no_longs                            ; (whether taken or not) ; _12_
  subq.w #1,d1                                                                 ;  _4_
  .clear_longs_loop:
    move.w (a0)+,d3                                                            ;  _8_
    move.l d4,(a1,d3.w)                                                        ; _20_
  dbra d1,.clear_longs_loop                                  ; (not taken: 16) ; _12_
  .clear_list_no_longs:
  tst.w d2                                                                     ;  _4_
  beq .get_vft_command                                ; (whether taken or not) ; _12_
  subq.w #1,d2                                                                 ;  _4_
  .clear_words_loop:
    move.w (a0)+,d3                                                            ;  _8_
    move.w d4,(a1,d3.w)                                                        ; _16_
  dbra d2,.clear_words_loop                                  ; (not taken: 16) ; _12_
  bra .get_vft_command                                                         ; _12_

;-- clear list
;------------------------------------------------------------------------------
;-- background list

.cmd_vft_background_list:
;    swap d2
;    swap d2
  move.w (a0)+,d2 ; number of words                                            ;  _8_
  tst.w d1                                                                     ;  _4_
  beq .background_list_no_longs                       ; (whether taken or not) ; _12_
  subq.w #1,d1                                                                 ;  _4_
  .background_longs_loop:
    move.w (a0)+,d3                                                            ;  _8_
    move.l (a2,d3.w),(a1,d3.w)                                                 ; _36_
  dbra d1,.background_longs_loop                             ; (not taken: 16) ; _12_
  .background_list_no_longs:
  tst.w d2                                                                     ;  _4_
  beq .get_vft_command                                ; (whether taken or not) ; _12_
  subq.w #1,d2                                                                 ;  _4_
  .background_words_loop:
    move.w (a0)+,d3                                                            ;  _8_
    move.w (a2,d3.w),(a1,d3.w)                                                 ; _28_
  dbra d2,.background_words_loop                             ; (not taken: 16) ; _12_
  bra .get_vft_command                                                         ; _12_

;-- background list
;------------------------------------------------------------------------------
;-- immediate data, longwords

.cmd_vft_imm_data_longword_list:
;    nop
;    nop
  subq.w #1,d1                                                                 ;  _4_
  .data_longs_loop:
    move.l (a0)+,d4                                                            ; _12_
    move.w (a0)+,d3                                                            ;  _8_
    move.l d4,(a1,d3.w)                                                        ; _20_
  dbra d1,.data_longs_loop                                   ; (not taken: 16) ; _12_
  bra .get_vft_command                                                         ; _12_

;-- immediate data, longwords
;------------------------------------------------------------------------------
;-- immediate data, words

.cmd_vft_imm_data_word_list:
;    nop
  subq.w #1,d1                                                                 ;  _4_
  .data_words_loop:
    move.w (a0)+,d4                                                            ;  _8_
    move.w (a0)+,d3                                                            ;  _8_
    move.w d4,(a1,d3.w)                                                        ; _16_
  dbra d1,.data_words_loop                                   ; (not taken: 16) ; _12_
  bra .get_vft_command                                                         ; _12_

;-- immediate data, words
;------------------------------------------------------------------------------
;-- register list, longwords

.cmd_vft_reg_data_longword_list:
  ;sysbreak
;    swap d7
;    swap d7
  move.w (a0)+,d2                                                              ;  _8_
  move.w (a0)+,d0                                                              ;  _8_
  move.l (a0)+,d4                                                              ; _12_
  tst.w d1                                                                     ;  _4_
  beq .reg_data_list_no_longs                         ; (whether taken or not) ; _12_
  subq.w #1,d1                                                                 ;  _4_
  .reg_data_longs_loop:
    move.w (a0)+,d3                                                            ;  _8_
    move.l d4,(a1,d3.w)                                                        ; _20_
  dbra d1,.reg_data_longs_loop                               ; (not taken: 16) ; _12_
  .reg_data_list_no_longs:
  tst.w d2                                                                     ;  _4_
  beq .reg_data_words_no_lower2                       ; (whether taken or not) ; _12_
  subq.w #1,d2                                                                 ;  _4_
  .reg_data_words_loop:
    move.w (a0)+,d3                                                            ;  _8_
    move.w d4,(a1,d3.w)                                                        ; _16_
  dbra d2,.reg_data_words_loop                               ; (not taken: 16) ; _12_
  .reg_data_words_no_lower2:
  tst.w d0                                                                     ;  _4_
  beq .get_vft_command                                ; (whether taken or not) ; _12_
  swap.w d4                                                                    ;  _4_
  subq.w #1,d0                                                                 ;  _4_
  .reg_data_words_loop_upper:
    move.w (a0)+,d3                                                            ;  _8_
    move.w d4,(a1,d3.w)                                                        ; _16_
  dbra d0,.reg_data_words_loop_upper                         ; (not taken: 16) ; _12_
  bra .get_vft_command                                                         ; _12_

;-- register list, longwords
;------------------------------------------------------------------------------
;-- register list, words

.cmd_vft_reg_data_word_list:
  ; d1 = upper loop counter
  move.w (a0)+,d2                                                              ;  _8_
  move.l (a0)+,d4                                                              ; _12_
  tst.w d2                                                                     ;  _4_
  beq .reg_data_words_no_lower                        ; (whether taken or not) ; _12_
  subq.w #1,d2                                                                 ;  _4_
  .reg_data_words_loop2:
    move.w (a0)+,d3                                                            ;  _8_
    move.w d4,(a1,d3.w)                                                        ; _16_
  dbra d2,.reg_data_words_loop2                              ; (not taken: 16) ; _12_
  .reg_data_words_no_lower:
  tst.w d1                                                                     ;  _4_
  beq .get_vft_command                                ; (whether taken or not) ; _12_
  swap.w d4                                                                    ;  _4_
  subq.w #1,d1                                                                 ;  _4_
  .reg_data_words_loop_upper2:
    move.w (a0)+,d3                                                            ;  _8_
    move.w d4,(a1,d3.w)                                                        ; _16_
  dbra d1,.reg_data_words_loop_upper2                        ; (not taken: 16) ; _12_
  bra .get_vft_command                                                         ; _12_

;-- register list, words
;------------------------------------------------------------------------------


.vft_command_jump_table:
  dc.l .cmd_vft_end_of_frame
  dc.l .cmd_vft_end_of_64k_block
  dc.l .cmd_vft_clear_list
  dc.l .cmd_vft_background_list
  dc.l .cmd_vft_imm_data_longword_list
  dc.l .cmd_vft_imm_data_word_list
  dc.l .cmd_vft_reg_data_longword_list
  dc.l .cmd_vft_reg_data_word_list



;###############################################################

