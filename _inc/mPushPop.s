
macro mPushAll
	movem.l a0-a6/d0-d7,-(sp)
endm

macro mPopAll
	movem.l (sp)+,a0-a6/d0-d7
endm


macro mPush reg01, reg02, reg03, reg04, reg05, reg06, reg07, reg08, reg09, reg10, reg11, reg12, reg13, reg14, reg15
  .if \#=0
    fail "Macro 'mPush' needs parameters!"
  .endif
  .if \#=1
    move.l \{reg01},-(sp)
  .endif
  .if \#=2
    movem.l \{reg01}/\{reg02},-(sp)
  .endif
  .if \#=3
    movem.l \{reg01}/\{reg02}/\{reg03},-(sp)
  .endif
  .if \#=4
    movem.l \{reg01}/\{reg02}/\{reg03}/\{reg04},-(sp)
  .endif
  .if \#=5
    movem.l \{reg01}/\{reg02}/\{reg03}/\{reg04}/\{reg05},-(sp)
  .endif
  .if \#=6
    movem.l \{reg01}/\{reg02}/\{reg03}/\{reg04}/\{reg05}/\{reg06},-(sp)
  .endif
  .if \#=7
    movem.l \{reg01}/\{reg02}/\{reg03}/\{reg04}/\{reg05}/\{reg06}/\{reg07},-(sp)
  .endif
  .if \#=8
    movem.l \{reg01}/\{reg02}/\{reg03}/\{reg04}/\{reg05}/\{reg06}/\{reg07}/\{reg09},-(sp)
  .endif
  .if \#=9
    movem.l \{reg01}/\{reg02}/\{reg03}/\{reg04}/\{reg05}/\{reg06}/\{reg07}/\{reg08}/\{reg09},-(sp)
  .endif
  .if \#=10
    movem.l \{reg01}/\{reg02}/\{reg03}/\{reg04}/\{reg05}/\{reg06}/\{reg07}/\{reg08}/\{reg09}/\{reg10},-(sp)
  .endif
  .if \#=11
    movem.l \{reg01}/\{reg02}/\{reg03}/\{reg04}/\{reg05}/\{reg06}/\{reg07}/\{reg08}/\{reg09}/\{reg10}/\{reg11},-(sp)
  .endif
  .if \#=12
    movem.l \{reg01}/\{reg02}/\{reg03}/\{reg04}/\{reg05}/\{reg06}/\{reg07}/\{reg08}/\{reg09}/\{reg10}/\{reg11}/\{reg12},-(sp)
  .endif
  .if \#=13
    movem.l \{reg01}/\{reg02}/\{reg03}/\{reg04}/\{reg05}/\{reg06}/\{reg07}/\{reg08}/\{reg09}/\{reg10}/\{reg11}/\{reg12}/\{reg13},-(sp)
  .endif
  .if \#=14
    movem.l \{reg01}/\{reg02}/\{reg03}/\{reg04}/\{reg05}/\{reg06}/\{reg07}/\{reg08}/\{reg09}/\{reg10}/\{reg11}/\{reg12}/\{reg13}/\{reg14},-(sp)
  .endif
  .if \#=15
    movem.l \{reg01}/\{reg02}/\{reg03}/\{reg04}/\{reg05}/\{reg06}/\{reg07}/\{reg08}/\{reg09}/\{reg10}/\{reg11}/\{reg12}/\{reg13}/\{reg14}/\{reg15},-(sp)
  .endif
endm

macro mPop reg01, reg02, reg03, reg04, reg05, reg06, reg07, reg08, reg09, reg10, reg11, reg12, reg13, reg14, reg15
  .if \#=0
    fail "Macro 'mPop' needs parameters!"
  .endif
  .if \#=1
    move.l (sp)+,\{reg01}
  .endif
  .if \#=2
    movem.l (sp)+,\{reg01}/\{reg02} ; yes, this wastes 4 cycles, but it also saves a little RAM
  .endif
  .if \#=3
    movem.l (sp)+,\{reg01}/\{reg02}/\{reg03}
  .endif
  .if \#=4
    movem.l (sp)+,\{reg01}/\{reg02}/\{reg03}/\{reg04}
  .endif
  .if \#=5
    movem.l (sp)+,\{reg01}/\{reg02}/\{reg03}/\{reg04}/\{reg05}
  .endif
  .if \#=6
    movem.l (sp)+,\{reg01}/\{reg02}/\{reg03}/\{reg04}/\{reg05}/\{reg06}
  .endif
  .if \#=7
    movem.l (sp)+,\{reg01}/\{reg02}/\{reg03}/\{reg04}/\{reg05}/\{reg06}/\{reg07}
  .endif
  .if \#=8
    movem.l (sp)+,\{reg01}/\{reg02}/\{reg03}/\{reg04}/\{reg05}/\{reg06}/\{reg07}/\{reg09}
  .endif
  .if \#=9
    movem.l (sp)+,\{reg01}/\{reg02}/\{reg03}/\{reg04}/\{reg05}/\{reg06}/\{reg07}/\{reg08}/\{reg09}
  .endif
  .if \#=10
    movem.l (sp)+,\{reg01}/\{reg02}/\{reg03}/\{reg04}/\{reg05}/\{reg06}/\{reg07}/\{reg08}/\{reg09}/\{reg10}
  .endif
  .if \#=11
    movem.l (sp)+,\{reg01}/\{reg02}/\{reg03}/\{reg04}/\{reg05}/\{reg06}/\{reg07}/\{reg08}/\{reg09}/\{reg10}/\{reg11}
  .endif
  .if \#=12
    movem.l (sp)+,\{reg01}/\{reg02}/\{reg03}/\{reg04}/\{reg05}/\{reg06}/\{reg07}/\{reg08}/\{reg09}/\{reg10}/\{reg11}/\{reg12}
  .endif
  .if \#=13
    movem.l (sp)+,\{reg01}/\{reg02}/\{reg03}/\{reg04}/\{reg05}/\{reg06}/\{reg07}/\{reg08}/\{reg09}/\{reg10}/\{reg11}/\{reg12}/\{reg13}
  .endif
  .if \#=14
    movem.l (sp)+,\{reg01}/\{reg02}/\{reg03}/\{reg04}/\{reg05}/\{reg06}/\{reg07}/\{reg08}/\{reg09}/\{reg10}/\{reg11}/\{reg12}/\{reg13}/\{reg14}
  .endif
  .if \#=15
    movem.l (sp)+,\{reg01}/\{reg02}/\{reg03}/\{reg04}/\{reg05}/\{reg06}/\{reg07}/\{reg08}/\{reg09}/\{reg10}/\{reg11}/\{reg12}/\{reg13}/\{reg14}/\{reg15}
  .endif
endm


macro mPushA0
	move.l a0,-(sp)
endm

macro mPushA1
	move.l a1,-(sp)
endm

macro mPushA2
	move.l a2,-(sp)
endm

macro mPushA3
	move.l a3,-(sp)
endm

macro mPushA4
	move.l a4,-(sp)
endm

macro mPushA5
	move.l a5,-(sp)
endm

macro mPushA6
	move.l a6,-(sp)
endm

macro mPopA0
	move.l (sp)+,a0
endm

macro mPopA1
	move.l (sp)+,a1
endm

macro mPopA2
	move.l (sp)+,a2
endm

macro mPopA3
	move.l (sp)+,a3
endm

macro mPopA4
	move.l (sp)+,a4
endm

macro mPopA5
	move.l (sp)+,a5
endm

macro mPopA6
	move.l (sp)+,a6
endm


macro mPushD0
	move.l d0,-(sp)
endm

macro mPushD1
	move.l d1,-(sp)
endm

macro mPushD2
	move.l d2,-(sp)
endm

macro mPushD3
	move.l d3,-(sp)
endm

macro mPushD4
	move.l d4,-(sp)
endm

macro mPushD5
	move.l d5,-(sp)
endm

macro mPushD6
	move.l d6,-(sp)
endm

macro mPushD7
	move.l d7,-(sp)
endm

macro mPopD0
	move.l (sp)+,d0
endm

macro mPopD1
	move.l (sp)+,d1
endm

macro mPopD2
	move.l (sp)+,d2
endm

macro mPopD3
	move.l (sp)+,d3
endm

macro mPopD4
	move.l (sp)+,d4
endm

macro mPopD5
	move.l (sp)+,d5
endm

macro mPopD6
	move.l (sp)+,d6
endm

macro mPopD7
	move.l (sp)+,d7
endm

