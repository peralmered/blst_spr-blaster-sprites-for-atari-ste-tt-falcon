# makefile ST v0.36 for GNU make

###############################################################################################
###############################################################################################
## Variables


###############################################################################################
## Stub name - must be TOS filename compliant (8 chars, no file ending)


STUBNAME=blst_spr


## Stub name - must be TOS filename compliant (8 chars, no file ending)
###############################################################################################
## Paths/filenames


# D8AOUT Expects ".nnn" at end during usage. Example:  cp $(TARGET) $(D8AOUT).001
D8AOUT=_RELEASE/$(STUBNAME).d8a/$(STUBNAME)

# Final executable
RELEASE_EXE=_RELEASE/$(STUBNAME).tos


## Paths/filenames
###############################################################################################
## Programs


# Include programs from separate file, so other people can have other paths
include Makefile_Programs.fixed

# Example:
# PYTHON=/cygdrive/d/Projekt/_Tools/XA/xa07/envxa07/Scripts/python.exe
# GIF2XA=$(PYTHON) \\Projekt\\_Tools\\XA\\xa07\\xa07.py
# GIF2STE=$(PYTHON) \\Projekt\\_Tools\\gif2ste.py
# CONV_AKS=/cygdrive/d/Projekt/STEDISK/s_rmac/thetwist/conv_aks.bat
# RMAC=/cygdrive/d/Projekt/SourceTree/rmac_shamus/rmac.exe
# UPX=/cygdrive/d/Projekt/_Tools/upx.exe


## Programs
###############################################################################################
## Other stuff


STARTINBACKGROUND=&


## Other stuff
###############################################################################################


## Variables
###############################################################################################
###############################################################################################
## Targets


#------------------------------------------------------------------------------
#-- all


all: main.tos


#--
#------------------------------------------------------------------------------
#-- pack


pack: upx


#--
#------------------------------------------------------------------------------
#-- rebuild


rebuild: clean all


#--
#------------------------------------------------------------------------------
#-- clean


clean:
	@echo Cleaning out executable files...
	@rm -f main.tos
	@echo Done!


	@echo Cleaning out graphics files...
	@rm -f bios_logo.xa
	@rm -f bfly_016x017.1bp
	@rm -f bfly_032x033.1bp
	@rm -f bfly_048x050.1bp
	@rm -f bfly_064x066.1bp
	@rm -f bfly_080x084.1bp
	@rm -f bfly_096x099.1bp
	@rm -f bfly_112x116.1bp
	@rm -f bfly_128x132.1bp
	@rm -f bfly_144x149.1bp
	@rm -f bfly_160x164.1bp
	@rm -f bflymask_016x017.1bp
	@rm -f bflymask_032x033.1bp
	@rm -f bflymask_048x050.1bp
	@rm -f bflymask_064x066.1bp
	@rm -f bflymask_080x084.1bp
	@rm -f bflymask_096x099.1bp
	@rm -f bflymask_112x116.1bp
	@rm -f bflymask_128x132.1bp
	@rm -f bflymask_144x149.1bp
	@rm -f bflymask_160x164.1bp
	@rm -f bfly_160x164_4bpl.4bp
	@rm -f bfly_160x164_4bpl.pal
	@rm -f background.4bp
	@echo Done!


	@echo Cleaning out audio files...
	@echo Done!


	@echo Cleaning out temp files...
	@echo Done!


#-- clean
#------------------------------------------------------------------------------
#-- main.tos


main.tos: bios_logo.xa \
          bfly_160x164.1bp \
          background.4bp 


	@echo
	@#    0         1         2         3         4         5         6         7
	@#    01234567890123456789012345678901234567890123456789012345678901234567890123456789
	@echo ---[ building main.tos ]-----------------------------------------------------
	@rm -f main.tos
	@rm -f $(RELEASE_EXE)
	@$(RMAC) -p -v main.s -o main.tos
	@# -px = extended debug symbols, -ps = standard debug symbols, -p = no debug symbols  $(RMAC) -p -v main.s -o main.tos
	@chmod 777 main.tos
	@cp main.tos $(RELEASE_EXE)
	@#    0         1         2         3         4         5         6         7
	@#    01234567890123456789012345678901234567890123456789012345678901234567890123456789
	@echo ---[ done ]------------------------------------------------------------------


#-- main.tos
#------------------------------------------------------------------------------


## Targets
###############################################################################################
###############################################################################################
## Files 




#------------------------------------------------------------------------------
#-- background.4bp

background.4bp: SOURCE=_gfx/background.gif
background.4bp: TARGET=$@
background.4bp:
	@echo
  @echo +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  @echo ++
  @echo ++ $(TARGET)
  @echo ++
  @$(GIF2STE) -i $(SOURCE) --autooutfile --strippath --nopal --bitplanes 4


#-- background.4bp
#------------------------------------------------------------------------------
#-- bfly_160x164.1bp


bfly_160x164.1bp: SOURCE=_gfx/bfly_160x164.gif
bfly_160x164.1bp: TARGET=bfly_160x164
bfly_160x164.1bp:
	@echo
  @echo +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  @echo ++
  @echo ++ $(TARGET)
  @echo ++

  @$(GIF2STE) -i _gfx/bfly_016x017.gif -ao -sp -np -b 1
  @$(GIF2STE) -i _gfx/bfly_032x033.gif -ao -sp -np -b 1
  @$(GIF2STE) -i _gfx/bfly_048x050.gif -ao -sp -np -b 1
  @$(GIF2STE) -i _gfx/bfly_064x066.gif -ao -sp -np -b 1
  @$(GIF2STE) -i _gfx/bfly_080x084.gif -ao -sp -np -b 1
  @$(GIF2STE) -i _gfx/bfly_096x099.gif -ao -sp -np -b 1
  @$(GIF2STE) -i _gfx/bfly_112x116.gif -ao -sp -np -b 1
  @$(GIF2STE) -i _gfx/bfly_128x132.gif -ao -sp -np -b 1
  @$(GIF2STE) -i _gfx/bfly_144x149.gif -ao -sp -np -b 1
  @$(GIF2STE) -i _gfx/bfly_160x164.gif -ao -sp -np -b 1
  @$(GIF2STE) -i _gfx/bfly_160x164_4bpl.gif -ao -sp -b 4 -ni

  @$(GIF2STE) -i _gfx/bflymask_016x017.gif -ao -sp -np -b 1
  @$(GIF2STE) -i _gfx/bflymask_032x033.gif -ao -sp -np -b 1
  @$(GIF2STE) -i _gfx/bflymask_048x050.gif -ao -sp -np -b 1
  @$(GIF2STE) -i _gfx/bflymask_064x066.gif -ao -sp -np -b 1
  @$(GIF2STE) -i _gfx/bflymask_080x084.gif -ao -sp -np -b 1
  @$(GIF2STE) -i _gfx/bflymask_096x099.gif -ao -sp -np -b 1
  @$(GIF2STE) -i _gfx/bflymask_112x116.gif -ao -sp -np -b 1
  @$(GIF2STE) -i _gfx/bflymask_128x132.gif -ao -sp -np -b 1
  @$(GIF2STE) -i _gfx/bflymask_144x149.gif -ao -sp -np -b 1
  @$(GIF2STE) -i _gfx/bflymask_160x164.gif -ao -sp -np -b 1


#-- bfly_160x164.1bp
#------------------------------------------------------------------------------
#-- bios_logo.xa


bios_logo.xa: SOURCE=_gfx/bios_logo.gif
bios_logo.xa: TARGET=$@
bios_logo.xa:
	@echo
  @echo +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  @echo ++
  @echo ++ $(TARGET)
  @echo ++
  @$(GIF2XA) -i $(SOURCE) -o $(TARGET)


#-- bios_logo.xa
#------------------------------------------------------------------------------


## Files 
###############################################################################################
###############################################################################################


