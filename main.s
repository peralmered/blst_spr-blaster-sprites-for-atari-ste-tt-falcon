;//////////////////////////////////////////////////////////////////////////////////////////////
;// ToDo & Done
;
;
; https://bitbucket.org/peralmered/blst_spr-blaster-sprites-for-atari-ste-tt-falcon/issues?status=new&status=open
;
;
;// ToDo & Done
;//////////////////////////////////////////////////////////////////////////////////////////////
;// Notes
;
;
; # bpl 0 = sprite, bpl 1 = mask
;
;
;// Notes
;//////////////////////////////////////////////////////////////////////////////////////////////


;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;//
;// Assembly-time helpers


  .68000
print ""

macro mFail errorstring
  ;      0         1         2         3         4         5         6         7
  ;      01234567890123456789012345678901234567890123456789012345678901234567890123456789
  print "---[ FATAL ERROR ]-----------------------------------------------------------"
  print \{errorstring}
  print ""
  .error
endm


;------------------------------------------------------------------------------


macro mWarn warningstring
  ;      0         1         2         3         4         5         6         7
  ;      01234567890123456789012345678901234567890123456789012345678901234567890123456789
  print "---[ WARNING ]---------------------------------------------------------------"
  print \{warningstring}
  print ""
  .error
endm


;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;// Feeling brave? Then by all means change these.


CPU_MODEL_68030=0
CPU_MODEL_68000=1


;// Feeling brave? Then by all means change these.
;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////



;// Assembly-time helpers
;//
;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;//
;// Defines and macros


; During development, waiting during "system test" intro gets old FAST, so...
SPEED_UP_SYSTEM_TEST equ 1


;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;// Features


; System test
DO_SYSTEM_TEST equ 1
SYSTEM_TEST_USE_PROPFONT equ 1 ; requires propfont, obviously

; Debugging
RASTERS_ENABLE equ 1
STEEM_DEBUG_ENABLE equ 1

; String output
PRINTVAL_ENABLE equ 0
SYSPRINT_ENABLE set 0
PROPFONT_ENABLE equ 1

; Compression
ICE_ENABLE  equ 0
ARJ_ENABLE  equ 0
LZ77_ENABLE equ 0

; Animation
XA_ENABLE equ 1
XA_VERSION equ 7

PALETTE_FADING_ENABLE equ 1
FILE_LOAD_ENABLE equ 1
DMA_SOUND_ENABLE equ 1
MUSIC_ENABLE equ 1
PIXELS_ENABLE equ 0
MATH_ENABLE equ 0
HBL_ENABLE equ 0
MEGASTE_ENABLE equ 1

TIMER_C_DISABLE_ON_STARTUP equ 0  ; This places a blank RTE on $114, disabling timer C safely.
                                  ; To re-enable at runtime, use macros "mTimercOn" and "mTimercOff"

MALLORY_ENABLE equ 0  ; Atari ST/STE memory manager
BLASTER_ENABLE equ 1

;// Features
;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;// Blaster operations


if BLASTER_ENABLE

;  BLASTER_HOP_ONES                    equ %00000000
;  BLASTER_HOP_HALFTONE                equ %00000001
;  BLASTER_HOP_SOURCE                  equ %00000010
;  BLASTER_HOP_SOURCE_AND_HALFTONE     equ %00000011
;
;  BLASTER_OP_SOURCE                   equ %00000011
;  BLASTER_OP_SOURCE_AND_TARGET        equ %00000001
;  BLASTER_OP_SOURCE_AND_NOT_TARGET    equ %00000010
;  BLASTER_OP_SOURCE_OR_TARGET         equ %00000111
;  BLASTER_OP_SOURCE_XOR_TARGET        equ %00000110
;  BLASTER_OP_SOURCE_NOT_TARGET        equ %00000100
;  BLASTER_OP_ZEROES                   equ %00000000
;  BLASTER_OP_ONES                     equ %00001111
;
;  BLASTER_COMMAND_START_HOG_MODE      equ %11000000
;  BLASTER_COMMAND_START_SHARED_MODE   equ %10000000

    .data
  blaster_sprite_leftmasks:
    dc.w %1111111111111111
    dc.w %0111111111111111
    dc.w %0011111111111111
    dc.w %0001111111111111
    dc.w %0000111111111111
    dc.w %0000011111111111
    dc.w %0000001111111111
    dc.w %0000000111111111
    dc.w %0000000011111111
    dc.w %0000000001111111
    dc.w %0000000000111111
    dc.w %0000000000011111
    dc.w %0000000000001111
    dc.w %0000000000000111
    dc.w %0000000000000011
    dc.w %0000000000000001


  blaster_sprite_rightmasks:
    dc.w %0000000000000000
    dc.w %1000000000000000
    dc.w %1100000000000000
    dc.w %1110000000000000
    dc.w %1111000000000000
    dc.w %1111100000000000
    dc.w %1111110000000000
    dc.w %1111111000000000
    dc.w %1111111100000000
    dc.w %1111111110000000
    dc.w %1111111111000000
    dc.w %1111111111100000
    dc.w %1111111111110000
    dc.w %1111111111111000
    dc.w %1111111111111100
    dc.w %1111111111111110
    .68000

endif


;// Blaster operations
;//////////////////////////////////////////////////////////////////////////////////////////////
;// Consequences of features


;//////////////////////////////////////////////////////////////////////////////
;// System test


if DO_SYSTEM_TEST
  SYSPRINT_ENABLE set 1
  if SYSTEM_TEST_USE_PROPFONT
    PROPFONT_ENABLE set 1
  endif
endif


;// System test
;//////////////////////////////////////////////////////////////////////////////
;// PRINTVAL


if PRINTVAL_ENABLE
  PRINTVAL_VALUEBUFFER_SIZE set 5
  PRINTVAL_VALUEBUFFER_SIZE_MAX set 16
endif  ; if PRINTVAL_ENABLE


;// PRINTVAL
;//////////////////////////////////////////////////////////////////////////////
;// PROPFONT


if SYSTEM_TEST_USE_PROPFONT
  f set 0
  PROPFONT_FONT_PER02=f
  f set f+1
  PROPFONT_FONT_PER=f
  f set f+1
  PROPFONT_FONT_MV_BOLI=f
  f set f+1
  PROPFONT_FONT_PALATINO=f
  f set f+1
  PROPFONT_FONT_CALIBRI_LIGHT=f
  f set f+1
  PROPFONT_FONT_MACINTOSH_1984=f
  f set f+1
  PROPFONT_FONT_ZX_SPECTRUM=f
  f set f+1
  PROPFONT_FONT_MSX=f
  f set f+1
  PROPFONT_FONT_ATARI_8BIT=f
  f set f+1
  
  PROPFONT_FONT_SELECT=PROPFONT_FONT_ZX_SPECTRUM
endif  ; if SYSTEM_TEST_USE_PROPFONT


if PROPFONT_ENABLE
  PROPFONT_FONTHEIGHT equ 14 ; pixels per line in font
  PROPFONT_FONTSPACEWIDTH equ 4 ; width of space char
  PROPFONT_FONTCHARSLINES equ 11 ; 13 ; lines of chars in font
  PROPFONT_FONTCHARSPERLINE equ 10 ; chars per line in font
  PROPFONT_FONTWIDTHCONVERTERTABLESIZE equ 17 ; value pairs in table
endif  ; if PROPFONT_ENABLE


;// PROPFONT
;//////////////////////////////////////////////////////////////////////////////


;// Consequences of features
;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;// Catching illegal combinations of feature EQU's


; PROPFONT
if SYSTEM_TEST_USE_PROPFONT
  if PROPFONT_ENABLE=0
    mFail "SYSTEM_TEST_USE_PROPFONT is enabled, but that requires PROPFONT_ENABLE to be set to 1 too!"
  endif  ; if PROPFONT_ENABLE
endif  ; if SYSTEM_TEST_USE_PROPFONT


;// Catching illegal combinations of feature EQU's
;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////


;// Defines and macros
;//
;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;//
;// Macros. Macrii. Macroies? Meh.


;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;// Pushing and popping


	include "_inc\\mPushPop.s"
	; mPushAll
	; mPopAll
	; mPush register[, register, register [...]]
	; mPop register[, register, register [...]]
	; mPushAn
	; mPopAn
	; mPushDn
	; mPopDn


;// Pushing and popping
;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;// DMA sound


; Macros and defines
;     mDmaPlay ratecode, startcode, pStart, pEnd
;         ratecodes:
;             DMA_6KHZ_STEREO,  DMA_6KHZ_MONO
;             DMA_12KHZ_STEREO, DMA_12KHZ_MONO
;             DMA_25KHZ_STEREO, DMA_25KHZ_MONO
;             DMA_50KHZ_STEREO, DMA_50KHZ_MONO
;         startcodes
;             DMA_START_LOOP_ON, DMA_START_LOOP_OFF
;         "pStart" and "pEnd" are addresses
;     mDmaSetStart pStart
;     mDmaStop


;------------------------------------------------------------------------------


if DMA_SOUND_ENABLE
  ; $ffff8921.b:
  DMA_RATE_ADDRESS equ $ffff8921
  DMA_6KHZ_STEREO  equ 0
  DMA_6KHZ_MONO    equ 128
  DMA_12KHZ_STEREO equ 1
  DMA_12KHZ_MONO   equ 129
  DMA_25KHZ_STEREO equ 2
  DMA_25KHZ_MONO   equ 130
  DMA_50KHZ_STEREO equ 3
  DMA_50KHZ_MONO   equ 131
  
  ; $ffff8900.w:
  DMA_PLAY_ADDRESS   equ $ffff8901
  DMA_START_LOOP_ON  equ 3
  DMA_START_LOOP_OFF equ 1
  DMA_STOP           equ 0
  
  DMA_SAMPLE_START_ADDRESS equ $ffff8901 ; must be movep.l
  DMA_SAMPLE_END_ADDRESS   equ $ffff890d ; must be movep.l


  ;----------------------------------------------------------------------------


  macro mDmaPlay ratecode, startcode, pStart, pEnd
  ; Usage: mDmaPlay ratecode, startcode, pStart, pEnd
    mPush d0, a0, a1
    lea \{pStart},a0
    move.l a0,d0
    move.l #DMA_SAMPLE_START_ADDRESS,a1
    movep.l d0,(0,a1)
    lea \{pEnd},a0
    move.l a0,d0
    move.l #DMA_SAMPLE_END_ADDRESS,a1
    movep.l d0,(0,a1)
    move.b #\{ratecode},DMA_RATE_ADDRESS
    move.b #\{startcode},DMA_PLAY_ADDRESS
    mPop d0, a0, a1
  endm
  
  
  ;----------------------------------------------------------------------------


  macro mDmaSetStart pStart
  ; Usage: mDmaSetStart pStart
    mPush d0, d1, a0, a1
    move.b DMA_PLAY_ADDRESS,d1
    swap d1
    lsl.l #8,d1
    and.l #$ff000000,d1
    lea \{pStart},a0
    move.l a0,d0
    or.l d1,d0
    move.l #DMA_SAMPLE_START_ADDRESS,a1
    movep.l d0,(0,a1)
    mPop d0, d1, a0, a1
  endm
  
  
  ;----------------------------------------------------------------------------


  macro mDmaStop
    move.b #DMA_STOP,DMA_PLAY_ADDRESS
  endm

endif  ; if DMA_SOUND_ENABLE


;// DMA sound
;//////////////////////////////////////////////////////////////////////////////////////////////
;// File loading


; Macros
;     mFileLoad file
;         "file" is address to load file to
;         requires "file"_filename: filename string, preferably placed with mFileLoadPlaceFilename
;     mFileLoadPlaceFilename filename
;         "filename" is string in quotes. Path, null and even are added automatically


;------------------------------------------------------------------------------


if FILE_LOAD_ENABLE

  macro mFileLoad pFile
  ; Usage: mFileLoad [label]
  ; Requires: label: [label]_filename - address to zero-terminated string
  ;                                     containing filename with full path
    lea \{pFile}_filename,a0
    lea \{pFile},a1
    jsr file_load_loadfile
    tst.l d0
    beq .noerror\~
    jmp file_load_error
  .noerror\~:
  endm


  ;----------------------------------------------------------------------------

  
  macro mFileLoadAddPath
    dc.b "prod.d8a\\\\"
  endm


  ;----------------------------------------------------------------------------

  
  macro mFileLoadPlaceFilename filename
  ; Usage: file_load_place_filename [filename in quotes]
    mFileLoadAddPath
    dc.b \{filename}
    dc.b 0
    even
  endm

endif  ; if FILE_LOAD_ENABLE


;// File loading
;//////////////////////////////////////////////////////////////////////////////////////////////
;// ARJ mode 7


; Macros
;     mDeArjLeaLea pArjData, pDest
;     mDeArjLeaScreen pArjData
;     mDeArjLeaSmokescreen pArjData


;------------------------------------------------------------------------------


if ARJ_ENABLE

  macro mDeArjLeaLea pArjData, pDest
  	mPushAll
  	move.l #\{pArjData}_size,d0
  	lea \{pArjData},a1
  	lea \{pDest},a0
  	jsr dearj
  	mPopAll
  endm


  ;----------------------------------------------------------------------------


  macro mDeArjLeaScreen pArjData
  	mPushAll
  	move.l #\{pArjData}_size,d0
  	lea \{pArjData},a1
  	move.l screenaddress,a0
  	jsr dearj
  	mPopAll
  endm


  ;----------------------------------------------------------------------------


  macro mDeArjLeaSmokescreen pArjData
  	mPushAll
  	move.l #\{pArjData}_size,d0
  	lea \{pArjData},a1
  	move.l smokescreenaddress,a0
  	jsr dearj
  	mPopAll
  endm

endif  ; if ARJ_ENABLE


;// ARJ mode 7
;//////////////////////////////////////////////////////////////////////////////////////////////
;// Sysprint


; Macros
;     mSysprint pString
;     mSysprint_with_linebreak pString


;------------------------------------------------------------------------------


if SYSPRINT_ENABLE

  macro mSysprintA1
    move.l screenaddress,a0
    if PROPFONT_ENABLE
      bsr propfont_draw_string
    else
      bsr sysprint_print_string
    endif
  endm


  ;----------------------------------------------------------------------------
  
  
  macro mSysprintA1Linebreak
    move.l screenaddress,a0
    if PROPFONT_ENABLE
      bsr propfont_draw_string_with_linebreak
    else
      bsr sysprint_print_string
    endif
  endm
  
  
  ;----------------------------------------------------------------------------


  macro mSysprint pString
    lea \{pString},a1
    mSysprintA1Linebreak
  endm


  ;----------------------------------------------------------------------------


  macro mSysprintLinebreak pString
    lea \{pString},a1
    mSysprint_a1
  endm


  ;----------------------------------------------------------------------------


  if PRINTVAL_ENABLE

    macro mSysprintDec number
      mPushAll
      move.l \{number},d0
      bsr printval_d0_dec
      mPopAll
    endm

    
    ;--------------------------------------------------------------------------


    macro mSysprintHex number
      mPushAll
      move.l \{number},d0
      bsr printval_d0_hex
      mPopAll
    endm

  endif ; if PRINTVAL_ENABLE

endif ; if SYSPRINT_ENABLE


;// Sysprint
;//////////////////////////////////////////////////////////////////////////////////////////////
;// MegaSTE


; Macros
;     mMegasteOn
;     mMegasteOff


;------------------------------------------------------------------------------


if MEGASTE_ENABLE

  macro mMegasteOn
    jsr turn_megaste_features_on
  endm


  ;----------------------------------------------------------------------------


  macro mMegasteOff
    jsr turn_megaste_features_off
  endm

endif  ; if MEGASTE_ENABLE


;// MegaSTE
;//////////////////////////////////////////////////////////////////////////////////////////////
;// Timer C stuff


; Macros
;     mTimercOn
;     mTimercOff


;------------------------------------------------------------------------------


macro mTimercOn
	move.w sr,-(sp)
	or.w #$0700,sr
	move.l #timer_c,$114
	move.w (sp)+,sr
endm


;------------------------------------------------------------------------------


macro mTimercOff
	move.w sr,-(sp)
	or.w #$0700,sr
  move.l save_timer_c,$114.w	
	move.w (sp)+,sr
endm


;// Timer C stuff
;//////////////////////////////////////////////////////////////////////////////////////////////
;// VBLinstall


; Macros
;     mVblInstall pRoutine
;     mVblUninstall


;------------------------------------------------------------------------------


macro mVblInstall pRoutine
  mPush a0
  lea \{pRoutine},a0
  move.l a0,vblextra
  mPop a0
endm


;------------------------------------------------------------------------------


macro mVblUninstall
  vblinstall blank_vbl
endm


;------------------------------------------------------------------------------

  .bss
  vblextra: ds.l 1
  .68000


;// VBLinstall
;//////////////////////////////////////////////////////////////////////////////////////////////
;// Internal VBL timer


; Macros
;     mClearTimer
;     mWaitForTimer time


;------------------------------------------------------------------------------


macro mClearTimer
	move.w #0,vblwaiter
endm


;------------------------------------------------------------------------------


macro mWaitForTimer time
	mPush d0
	move.w #\{time},d0
	jsr waitforvblwaiter
	mPop d0
endm


;// Internal VBL timer
;//////////////////////////////////////////////////////////////////////////////////////////////
;// Debugging


; Macros
;     mBreak
;     mMarkCurrentScanline
;     mBackCol color


;------------------------------------------------------------------------------


macro mBreak
  .iif STEEM_DEBUG_ENABLE, clr.b $ffffc123
endm


;------------------------------------------------------------------------------


macro mMarkCurrentScanline
  if RASTERS_ENABLE
    jsr mMarkCurrentScanline_code
  endif
endm


;------------------------------------------------------------------------------


macro mBackCol color
  if RASTERS_ENABLE
  	move.w #\{color},currentpalette
  	move.w #\{color},$ffff8240.w
  endif
endm


;// Debugging
;//////////////////////////////////////////////////////////////////////////////////////////////
;// Screen buffers


; Macros
;     mSwapScreens
;     mClearScreen
;     mClearScreenNoBlaster
;     mClearSmokescreen
;     mClearSmokescreenNoBlaster
;     mClear1Bpl pAddress
;     mClear1BplNoBlaster pAddress


;------------------------------------------------------------------------------


macro mSwapScreens
  jsr mSwapScreens_code
endm


;------------------------------------------------------------------------------


macro mClearScreen
  jsr clearscreen
endm


macro mClearScreenNoBlaster
  jsr clearscreennoblaster
endm


macro mClearSmokescreen
  jsr clearsmokescreen
endm


macro mClearSmokescreenNoBlaster
  jsr clearsmokescreennoblaster
endm


;------------------------------------------------------------------------------


macro mClear1Bpl pAddress
  mPush a0
  lea \{pAddress},a0
  jsr clear1bpl
  mPop a0
endm


macro mClear1BplNoBlaster pAddress
  mPush a0
  lea \{pAddress},a0
  jsr clear1bplnoblaster
  mPop a0
endm


;// Screen buffers
;//////////////////////////////////////////////////////////////////////////////////////////////
;// Palette stuff


; Macros
;     mSetPalette pPalette
;     mFadeToPalette pPalette, frames


;------------------------------------------------------------------------------


macro mSetPalette pPalette
	mPush a0
	lea \{pPalette},a0
	bsr mSetPalette_code
	mPop a0
endm


;------------------------------------------------------------------------------


if PALETTE_FADING_ENABLE
  macro mFadeToPalette pPalette, frames
  ; Usage: mFadeToPalette [address to palette] [number of frames between steps, minimum 1]
  	if \{frames}<1 ; disallow values lower than 1
    	mFail "Macro mFadeToPalette: Second parameter cant be lower than 1"
  	endif
  	movem.l a0/d0,-(sp)
  	lea \{pPalette},a0
  	move.l #\{frames}-1,d0
  	bsr fadepalettesaesthetic
  	movem.l (sp)+,a0/d0
 	endm
endif


;// Palette stuff
;//////////////////////////////////////////////////////////////////////////////////////////////
;// String thingies


macro mPutString pString
  dc.b \{pString}
  dc.b 0
  even
endm


;------------------------------------------------------------------------------


macro mPutStringCr pString
  dc.b \{pString}
  dc.b 13,0
  even
endm


;------------------------------------------------------------------------------


macro mPutString2Cr pString
  dc.b \{pString}
  dc.b 13,13,0
  even
endm


;------------------------------------------------------------------------------


macro mPutStringNoNull pString
  dc.b \{pString}
  even
endm


;// String thingies
;//////////////////////////////////////////////////////////////////////////////////////////////
;// Other macros


macro mNops number
  dcb.w \{number},$4e71
endm


;------------------------------------------------------------------------------


if ICE_ENABLE

  macro mDeiceLeaLea pIced, pDest
  	movem.l a0/a1,-(sp)
  	lea \{pIced},a0
  	lea \{pDest},a1
  	jsr deice
  	movem.l (sp)+,a0/a1
  endm

endif


;------------------------------------------------------------------------------


macro mVblWait frames
; Usage: mVblWait [number of frames to wait, minimum 1]
	.if \{frames}<1 ; disallow values lower than 1
	  mFail "Macro mVblWait needs parameter"
	.endif
	.if \{frames}=1 ; special case
	  jsr wait4vbl
  .else
  	mPush d0
    .if \{frames}<=128
  	  moveq #\{frames}-1,d0
    .else
      ; wait4vbld0 has a dbra so it can at most do 65535 iterations
      move.w #\{frames}-1,d0
    .endif
  	bsr wait4vbld0
  	mPop d0
	.endif
endm


;------------------------------------------------------------------------------


;macro copy32klealeanoblast source, dest
;	mPush a0
;	pusha1
;	lea \{source},a0
;	lea \{dest},a1
;	bsr copy32000noblaster
;	popa1
;	mPop a0
;endm
;
;
;macro copy32ktoscreenleanoblast source
;	mPush a0
;	pusha1
;	lea \{source},a0
;	move.l screenaddress,a1
;	bsr copy32000noblaster
;	popa1
;	mPop a0
;endm


;// Other macros
;//////////////////////////////////////////////////////////////////////////////////////////////


;// Macros. Macrii. Macroies? Meh.
;//
;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;//
;// Main


  bra.s past_hello
  	dc.b "  Per Almered/Excellence In Art 2018  "
  	dc.b "--  Based in part on the DHS demosystem  "
  	even
past_hello:


;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;// Init


  ; Supervisor mode
	clr.l -(sp)
	move.w #$20,-(sp)
	trap #1
	addq.l #6,sp
	move.l d0,oldusp

  ; Set up screen addresses
	lea smokescreen1,a0
	move.l a0,d0
	clr.b d0
	move.l d0,a0
	move.l a0,screenaddress
	add.l #32000,a0
	move.l a0,smokescreenaddress
	bsr clearsmokescreennoblaster
	bsr mSwapScreens_code
	bsr clearsmokescreennoblaster

  ; VBL install blank routine
	lea blank_vbl,a0
	move.l a0,vblextra

  if DO_SYSTEM_TEST
    bsr systemtest
  endif


;//////////////////////////////////////////////////////////////
;// start.s


  ; mshrink()
	move.l  4(sp),a5                ; address to basepage
	move.l  $0c(a5),d0              ; length of text segment
	add.l   $14(a5),d0              ; length of data segment
	add.l   $1c(a5),d0              ; length of bss segment
	add.l   #$1000,d0               ; length of stackpointer
	add.l   #$100,d0                ; length of basepage
	move.l  a5,d1                   ; address to basepage
	add.l   d0,d1                   ; end of program
	and.l   #-2,d1                  ; make address even
	move.l  d1,sp                   ; new stackspace

	move.l  d0,-(sp)                ; mshrink()
	move.l  a5,-(sp)
	move.w  d0,-(sp)
	move.w  #$4a,-(sp)
	trap    #1
	lea 	12(sp),sp

ignoremshrink:

  ; Save STE linewidth and scroll registers
	move.b $ffff8265,old8265
	move.b $ffff820f,old820f

  ; MegaSTE
this_is_ggntrap::
	move.l $8,-(sp)
	move.l #.ggntrap,$8
	move.b $ffff8e21,old8e21
.ggntrap:
	move.l (sp)+,$8

  ; Get screen rez
	move.w	#4,-(sp)
	trap	#14
	addq.l	#2,sp
	move.w	d0,oldrez
	
  ; Get physbase
	move #2,-(sp)		; get address of physical screen
	trap #14
	addq #2,sp
	move.l d0,physbase

  ; Get old palette
	movem.l $ffff8240,d0-d7
	movem.l d0-d7,oldpal

  ; Keyclick: first save, then turn off
	move.b $484,old484
	bclr #0,$484

  ; Hide mouse pointer, then disable mouse reporting
	dc.w $a00a
	move.b #$12,$fffffc02

  ; Set screen rez and address
	move.w #0,-(sp)				; set lowres
	move.l physbase,-(sp)
	move.l physbase,-(sp)
	move.w #5,-(sp)
	trap #14
	add.l #12,sp
	
  ; Set 50Hz
	bset.b #1,$ffff820a

; From DHS' demosystem
;--------------	Save vectors, MFP and start the demosystem
	move.w	#$2700,sr			;Stop interrupts

	move.l	usp,a0				;USP
	move.l	a0,save_usp			;
	
	move.l	$68.w,save_hbl			;HBL
	move.l	$70.w,save_vbl			;VBL
	move.l	$134.w,save_timer_a		;Timer-A
	move.l	$120.w,save_timer_b		;Timer-B
	move.l	$114.w,save_timer_c		;Timer-C
	move.l	$110.w,save_timer_d		;Timer-D
	move.l	$118.w,save_acia		;ACIA

	lea	save_mfp,a0			;Restore vectors and mfp
	move.b	$fffffa01.w,(a0)+		;// datareg
	move.b	$fffffa03.w,(a0)+		;Active edge
	move.b	$fffffa05.w,(a0)+		;Data direction
	move.b	$fffffa07.w,(a0)+		;Interrupt enable A
	move.b	$fffffa13.w,(a0)+		;Interupt Mask A
	move.b	$fffffa09.w,(a0)+		;Interrupt enable B
	move.b	$fffffa15.w,(a0)+		;Interrupt mask B
	move.b	$fffffa17.w,(a0)+		;Automatic/software end of interupt
	move.b	$fffffa19.w,(a0)+		;Timer A control
	move.b	$fffffa1b.w,(a0)+		;Timer B control
	move.b	$fffffa1d.w,(a0)+		;Timer C & D control
	move.b	$fffffa27.w,(a0)+		;Sync character
	move.b	$fffffa29.w,(a0)+		;USART control
	move.b	$fffffa2b.w,(a0)+		;Receiver status
	move.b	$fffffa2d.w,(a0)+		;Transmitter status
	move.b	$fffffa2f.w,(a0)+		;USART data

	move.l	#hbl,$68.w 			;Set HBL
	move.l	#vbl,$70.w 			;Set VBL

	move.l	#timer_a,$134.w			;Set Timer A
	move.l	#timer_b,$120.w			;Set Timer B
  if TIMER_C_DISABLE_ON_STARTUP
		move.l	#timer_c,$114.w			;Set Timer C
  endif
	move.l	#timer_d,$110.w			;Set Timer D
	move.l	#acia,$118.w			;Set ACIA

	clr.b	$fffffa07.w			;Interrupt enable A (Timer A & B)
	clr.b	$fffffa13.w			;Interrupt mask A (Timer A & B)
	clr.b	$fffffa09.w			;Interrupt enable B (Timer C & D)
	clr.b	$fffffa15.w			;Interrupt mask B (Timer C & D)

	clr.b	$fffffa19.w			;Timer A control (stop)
	clr.b	$fffffa1b.w			;Timer B control (stop)
	clr.b	$fffffa1d.w			;Timer C & D control (stop)

	bclr #3,$fffffa17.w			;Automatic end of interrupt
	bset #5,$fffffa07.w			;Interrupt enable A (Timer A)
	bset #5,$fffffa13.w			;Interrupt mask A

	move.w	#$2300,sr			;Enable interrupts

  ; ...and save stack pointer
	move.l sp,startupsp


;// start.s
;//////////////////////////////////////////////////////////////


  mMegasteOn
	
	bsr resetblaster

  bsr initall


;//////////////////////////////////////////////////////////////////////////////
;// "BIOS boot"

  
if DO_SYSTEM_TEST
  ; show bios logo
  lea bios_logo_xa,a0
  move.l screenaddress,a1
  bsr xa_unpack_one_frame
  ; print
  bsr systemtest_print

  .iif SPEED_UP_SYSTEM_TEST=0, mVblWait 10

  ; blank out screen
  .iif SPEED_UP_SYSTEM_TEST=0, mVblWait 35
  if SPEED_UP_SYSTEM_TEST
    mSetPalette allblackpal
  else
    mFadeToPalette allblackpal, 2
  endif
  bsr clearscreen
  .iif SPEED_UP_SYSTEM_TEST=0, mVblWait 25
  .iif PROPFONT_ENABLE, bsr propfont_reset_cursor


  ;----------------------------------------------------------------------------


  .data
  bios_logo_xa:
    incbin "bios_logo.xa"
  .68000

endif


;// "BIOS boot"
;//////////////////////////////////////////////////////////////////////////////


  mMegasteOff
  mVblWait 1


;//////////////////////////////////////////////////////////////////////////////
;// Start music


  if MUSIC_ENABLE
    jsr ym_music_init
    move.w #1,ym_music_enable
  endif


;// Start music
;//////////////////////////////////////////////////////////////////////////////


  move.l #0,framecounter
  mClearTimer


;// Init
;//////////////////////////////////////////////////////////////////////////////////////////////
;// Show. Road. Let's get one on the other!


  mSetPalette testpal
  mSetPalette gem_pal
  move.w #0,currentpalette

  mSetPalette butterfly_pal
  mSetPalette bfly_palette

START_X equ 40  ; (319-159)+1+15  ; 20 ;319+1  ; 319-160+1  ; 319-96+15  ; -2
START_Y equ 40

SPRITE_NUM_MAX equ 10

KEY_RIGHT equ $4d  ; $1e
KEY_LEFT equ $4b  ; $1f
KEY_UP equ $48
KEY_DOWN equ $50
KEY_SPACE equ $39

  move.w #START_X,myX
  move.w #START_Y,myY
  move.w #0,mySpriteNum


mainloop:

  mSwapScreens
  mVblWait 1

  ; Reset background
  lea background,a0
  move.l smokescreenaddress,a1
  bsr copy32000
  mMarkCurrentScanline

  move.w myX,d0
  move.w myY,d1

  move.w mySpriteNum,d2
  lea sprite_table,a6
  add.w d2,d2
  add.w d2,d2
  ext.l d2
  move.l (a6,d2),a0

  move.l smokescreenaddress,a1
  bsr blsp_draw_fullclip
  ;bsr blsp_draw_noclip

  mMarkCurrentScanline

  ;----------------------------------------------------------------------------
  ;-- Keyboard movement

  move.w myX,d0
  move.w myY,d1
  move.w mySpriteNum,d2

  move.b $fffffc02,d6
;  tst.b d6
;  beq .nokey
;    mBreak
;.nokey:

  cmp.b #KEY_RIGHT,d6
  bne .noright
    add.w #1,d0
.noright:
  cmp.b #KEY_LEFT,d6
  bne .noleft
    sub.w #1,d0
.noleft:
  cmp.b #KEY_DOWN,d6
  bne .nodown
    add.w #1,d1
.nodown:
  cmp.b #KEY_UP,d6
  bne .noup
    sub.w #1,d1
.noup:

  cmp.b #KEY_SPACE,d6
  bne .nospace
    cmp.w #SPRITE_NUM_MAX,d2
    beq .max_val
      add.w #1,d2
      bra.s .sprite_cycle_done
    .max_val:
      move.w #0,d2
  .sprite_cycle_done:
  mVblWait 6  ; unbelievably fugly fix for key repeat
.nospace:

  move.w d0,myX
  move.w d1,myY
  move.w d2,mySpriteNum
  bsr flush_keyboard

  ;-- Keyboard movement
  ;----------------------------------------------------------------------------

  bra mainloop


  .bss
myX:          ds.w 1
myY:          ds.w 1
mySpriteNum:  ds.w 1

  .data
sprite_table:
  dc.l bfly_016
  dc.l bfly_032
  dc.l bfly_048
  dc.l bfly_064
  dc.l bfly_080
  dc.l bfly_096
  dc.l bfly_112
  dc.l bfly_128
  dc.l bfly_144
  dc.l bfly_160
  dc.l bfly_160_4bpl

background:
  incbin "background.4bp"
  .68000

endloop:
  bra.s endloop


;// Show. Road. Let's get one on the other!
;//////////////////////////////////////////////////////////////////////////////////////////////
;// Exit


exitall:


;//////////////////////////////////////////////////////////////////////////////
;// Shut down music


  if MUSIC_ENABLE
    jsr ym_music_exit
  endif


;// Shut down music
;//////////////////////////////////////////////////////////////////////////////


;//////////////////////////////////////////////////////////////
;// end.s


  ; Shut down DMA audio
	move.w #0,$ffff8900

  ; Restore stack pointer
	move.l startupsp,sp
	
  ; Restore MegaSTE register
	move.l $8,-(sp)
	move.l #.ggntrap,$8
	move.b old8e21,$ffff8e21
.ggntrap:
	move.l (sp)+,$8

; From DHS' demosystem
	move.w	#$2700,sr			;Stop interrupts

	move.l	save_usp,a0			;USP
	move.l	a0,usp				;
	move.l	save_hbl,$68.w			;HBL
	move.l	save_vbl,$70.w			;VBL
	move.l	save_timer_a,$134.w		;Timer A
	move.l	save_timer_b,$120.w		;Timer B
	move.l	save_timer_c,$114.w		;Timer C
	move.l	save_timer_d,$110.w		;Timer D
	move.l	save_acia,$118.w		;ACIA

	lea	save_mfp,a0			;restore vectors and mfp
	move.b	(a0)+,$fffffa01.w		;// datareg
	move.b	(a0)+,$fffffa03.w		;Active edge
	move.b	(a0)+,$fffffa05.w		;Data direction
	move.b	(a0)+,$fffffa07.w		;Interrupt enable A
	move.b	(a0)+,$fffffa13.w		;Interupt Mask A
	move.b	(a0)+,$fffffa09.w		;Interrupt enable B
	move.b	(a0)+,$fffffa15.w		;Interrupt mask B
	move.b	(a0)+,$fffffa17.w		;Automatic/software end of interupt
	move.b	(a0)+,$fffffa19.w		;Timer A control
	move.b	(a0)+,$fffffa1b.w		;Timer B control
	move.b	(a0)+,$fffffa1d.w		;Timer C & D control
	move.b	(a0)+,$fffffa27.w		;Sync character
	move.b	(a0)+,$fffffa29.w		;USART control
	move.b	(a0)+,$fffffa2b.w		;Receiver status
	move.b	(a0)+,$fffffa2d.w		;Transmitter status
	move.b	(a0)+,$fffffa2f.w		;USART data

	move.w	#$2300,sr			;Start interrupts


  ; Restore screen rez and physbase
	move.w	oldrez,-(sp)
	move.l	physbase,-(sp)
	move.l	physbase,-(sp)
	move.w	#5,-(sp)
	trap	#14
	add.l	#12,sp

  ; Restore STE linewidth and scroll
	move.b old8265,$ffff8265
	move.b old820f,$ffff820f

  ; Testore palette
	movem.l oldpal,d0-d7
	movem.l d0-d7,$ffff8240

 ; Testore mouse reading, then mouse pointer
	move.b #$8,$ffffc02
	dc.w $a009

  ; Restore key click
	move.b old484,$484

  ; Back to User mode from Supervisor mode
	move.l oldusp(pc),-(sp)
	move.w #$20,-(sp)
	trap #1
	addq.l #6,sp

  ; pterm()
	clr.w -(sp)
	move.w #$4c,-(sp)
	trap #1

  .data
oldusp:
  dc.l 0
  .68000


;// end.s
;//////////////////////////////////////////////////////////////


;// Exit
;//////////////////////////////////////////////////////////////////////////////////////////////


;// Main
;//
;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;//
;// Subroutines


;//////////////////////////////////////////////////////////////////////////////////////////////
;// Initialize codegroups and tables


initall:
	bsr init_ytable

  ; sysprint
  if SYSPRINT_ENABLE  
    bsr sysprint_init
  endif

  ; propfont
  if PROPFONT_ENABLE
    bsr propfont_init
  endif

  ; default palette
	mSetPalette bwpal

  rts

;// Initialize codegroups and tables
;//////////////////////////////////////////////////////////////////////////////////////////////
;// Y table


init_ytable:
  lea ytable,a0
  clr.l d0
  move.l #200-1,d7
  .loop:
    move.l d0,(a0)+
    add.l #160,d0
  dbra d7,.loop
  rts


;------------------------------------------------------------------------------


  .bss
ytable:   ds.l 200
  .68000


;// Y table
;//////////////////////////////////////////////////////////////////////////////////////////////
;// Error


fatal_error:
  mBackCol $700
  mBackCol $300
  mBackCol $000
  bra fatal_error


;// Error
;//////////////////////////////////////////////////////////////////////////////////////////////
;// Music


if MUSIC_ENABLE

    .data
  ym_music:
    incbin "demosong.xym"
    .68000
  ym_music_init   equ ym_music
  ym_music_exit   equ ym_music+4
  ym_music_vbl    equ ym_music+8


  ;------------------------------------------------------------------------------


    .bss
  ym_music_enable:  ds.w 1
    .68000

endif  ; if MUSIC_ENABLE


;// Music
;//////////////////////////////////////////////////////////////////////////////////////////////
;// Keyboard


flush_keyboard:
.loop:
  btst.b #0,$fffffc00
  bne .loop
  rts


;// Keyboard
;//////////////////////////////////////////////////////////////////////////////////////////////
;// Blaster sprite (blsp)


;--------------------------------------------------------------
;-- Blaster EQUs

BLASTER_HOP_ONES                    equ %00000000
BLASTER_HOP_HALFTONE                equ %00000001
BLASTER_HOP_SOURCE                  equ %00000010
BLASTER_HOP_SOURCE_AND_HALFTONE     equ %00000011

BLASTER_OP_SOURCE                   equ %00000011
BLASTER_OP_SOURCE_AND_TARGET        equ %00000001
BLASTER_OP_SOURCE_AND_NOT_TARGET    equ %00000010
BLASTER_OP_SOURCE_OR_TARGET         equ %00000111
BLASTER_OP_SOURCE_XOR_TARGET        equ %00000110
BLASTER_OP_SOURCE_NOT_TARGET        equ %00000100
BLASTER_OP_ZEROES                   equ %00000000
BLASTER_OP_ONES                     equ %00001111

BLASTER_COMMAND_START_HOG_MODE      equ %11000000
BLASTER_COMMAND_START_SHARED_MODE   equ %10000000

;-- Blaster EQUs
;--------------------------------------------------------------
;-- Screen mode EQUs

SCREEN_WIDTH_PIXELS equ 320
SCREEN_WIDTH_BYTES equ 160
SCREEN_BITPLANES equ 4

;-- Screen mode EQUs
;--------------------------------------------------------------
;-- User setting EQUs

SCREEN_CLIP_Y_MIN equ 32
SCREEN_CLIP_Y_MAX equ 199-32
SCREEN_CLIP_X_MIN equ 32  ; must be on a 16-pixel-boundary!
SCREEN_CLIP_X_MAX equ 319-32  ; must be on a 16-pixel-boundary!

;-- User setting EQUs
;--------------------------------------------------------------
;-- Verify user settings

macro fatal_error errorstring
  print "---[ FATAL ERROR ]-----------------------------------------------------------"
  print \{errorstring}
  print ""
  print ""
  .error
endm

; Check clip values
if SCREEN_CLIP_Y_MIN<0
  fatal_error "SCREEN_CLIP_Y_MIN must be a positive value!"
endif
if SCREEN_CLIP_X_MIN<0
  fatal_error "SCREEN_CLIP_X_MIN must be a positive value!"
endif

if (SCREEN_CLIP_X_MIN & $f)!=0
  fatal_error "SCREEN_CLIP_X_MIN must be divisible by 16!"
endif
if ((SCREEN_CLIP_X_MAX+1) & $f)!=0
  fatal_error "SCREEN_CLIP_X_MAX must be divisible by 16!"
endif

; Check number of bitplanes, and create bitplane shift value to save a couple of multiplications
BPL_ERROR set 1
BPL_SHIFTER set 0
if SCREEN_BITPLANES=1
  BPL_ERROR set 0
  BPL_SHIFTER set 1 ; 2
endif
if SCREEN_BITPLANES=2
  BPL_ERROR set 0
  BPL_SHIFTER set 2 ; 4
endif
if SCREEN_BITPLANES=4
  BPL_ERROR set 0
  BPL_SHIFTER set 3 ; 8
endif
if SCREEN_BITPLANES=8
  BPL_ERROR set 0
  BPL_SHIFTER set 8 ; 256
endif
if BPL_ERROR=1
  fatal_error "SCREEN_BITPLANES must be 1, 2, 4 or 8!"
endif


;-- Verify user settings
;--------------------------------------------------------------

; Sprite struct:
;     Number_of_bitplanes.w
;     Width.w  ; pixels
;     Height.w  ; pixels
;     mask data, 1 bitplane
;     sprite data, 1-4 bitplanes, not interleaved


blsp_draw_fullclip:
; In:  d0.w - X
;      d1.w - Y
;      a0.l - pointer to sprite struct
;      a1.l - pointer to screen

  clr.b bslp_clip_x_min_flag
  clr.b bslp_clip_x_max_flag
  
  clr.b blsp_bl_extra_source_read_flag

  ;------------------------------------------------------------
  ;-- Setup blaster registers

  ; These are all default values
  move.w #2,blsp_bl_source_x_inc
  move.w #2,blsp_bl_source_y_inc
  move.w #-1,blsp_bl_endmask_1
  move.w #SCREEN_BITPLANES*2,blsp_bl_dest_x_inc

  ;-- Setup blaster registers
  ;------------------------------------------------------------
  ;-- Get width and height of sprite

  ; Get number of bitplanes in sprite
  move.w (a0)+,blsp_sprite_bitplanes
  ; Get sprite width
  move.w (a0)+,d7  ; let's not modify d7 for a while, it's going to see use later
  ; Get sprite height
  move.w (a0)+,d6  ; let's not touch a0 or d6, we're going to use them in a bit

  ;-- Get width and height of sprite
  ;------------------------------------------------------------
  ;-- "Early out" clipping

  ; Y max
  cmp.w #SCREEN_CLIP_Y_MAX,d1
  bgt .early_out
  ; X max
  cmp.w #SCREEN_CLIP_X_MAX,d0
  bgt .early_out
  ; Y min
  move.w #SCREEN_CLIP_Y_MIN,d2
  sub.w d6,d2
  cmp.w d2,d1
  ble .early_out
  ; X min
  move.w #SCREEN_CLIP_X_MIN,d2
  sub.w d7,d2
  cmp.w d2,d0
  ble .early_out

  ;-- "Early out" clipping
  ;------------------------------------------------------------
  ;-- Get width-in-words

  move.w d7,blsp_width_pixels_before_clip
  lsr.w #4,d7
  move.w d7,blsp_width_words_before_clip

  ;-- Get width-in-words
  ;------------------------------------------------------------
  ;-- X position

  move.w d0,d5  ; Back up X so we can still access the untouched value later
  and.b #$f,d5  ; Get lowest 4 bits from X for skewing
  move.b d5,blsp_skew_value
  ; Mask out so we get a clean multiple of 16
  
  move.w d0,d5
  sub.w #SCREEN_CLIP_X_MIN,d5
  tst.w d5
  bmi .x_position_done
    move.w d0,d4
;    ; Adjust screen pointer to correct "16-pixel block"
    lsr.w #4,d4
    lsl.w #BPL_SHIFTER,d4  ; instead of mulu #2*SCREEN_BITPLANES,d4
    add.w d4,a1
  .x_position_done:

  ;-- X position
  ;------------------------------------------------------------
  ;-- Partial clipping

  ; Write "before clipping" width and height values
  move.w d6,blsp_height_pixels_before_clip
  move.w d6,blsp_height_pixels_after_clip  ; default value, in case no clipping happens
  move.w d7,blsp_width_words_after_clip  ; default value


  ; Y max clipping
  move.w #SCREEN_CLIP_Y_MAX,d2
  move.w d1,d3  ; d3=Y
  add.w d6,d3   ; add sprite height to d3...
  sub.w d3,d2  ; subtract that from clip value. If result<0, it's clipping time
  bpl.s .no_y_max_clip
    neg.w d2  ; overshoot in d2
    sub.w #1,d2
    sub.w d2,d6
    move.w d6,blsp_height_pixels_after_clip
  .no_y_max_clip:


  ; Y min clipping
  move.w #SCREEN_CLIP_Y_MIN,d2
  sub.w d1,d2
  bmi.s .no_y_min_clip
    sub.w d2,d6
    move.w d6,blsp_height_pixels_after_clip
    ; add offset to mask/sprite data
    move.w d7,d3  ; offset = number of words in width...
    add.w d3,d3  ; ...times 2...
    mulu d3,d2  ; ...times the number of lines
    add.w d2,a0  ; add offset
    ; Adjust Y to clipping
    move.w #SCREEN_CLIP_Y_MIN,d1
.no_y_min_clip:


  ; X max clipping
  move.w #SCREEN_CLIP_X_MAX,d2
  sub.w blsp_width_pixels_before_clip,d2
  add.w #1,d2
  cmp.w d2,d0
  ble.s .no_x_max_clip
    move.b #1,bslp_clip_x_max_flag
    ; Calculate overshoot
    move.w d0,d3
    add.w blsp_width_pixels_before_clip,d3
    sub.w #SCREEN_CLIP_X_MAX,d3
    sub.w #1,d3  ; d3 is now overshoot

    move.w blsp_width_pixels_before_clip,d7
    sub.w d3,d7
    lsr.w #4,d7
    move.w d7,blsp_width_words_after_clip

    lsr.w #3,d3
    add.w d3,blsp_bl_source_y_inc

    ; Adjust for skew value changing width
    tst.b blsp_skew_value
    beq .no_fix_src_y_inc
      add.w #2,blsp_bl_source_y_inc
    .no_fix_src_y_inc:

  .no_x_max_clip:


  ; X min clipping
  move.w #SCREEN_CLIP_X_MIN,d2
  cmp.w d2,d0
  bge.s .no_x_min_clip
    move.b #1,bslp_clip_x_min_flag

    ; Calculate undershoot
    move.w d0,d7
    sub.w #SCREEN_CLIP_X_MIN,d7
    
    neg.w d7  ; make d7 positive
    lsr.w #4,d7  ; multiply by 16, because word = 16 pixels
    sub.w d7,blsp_width_words_after_clip  ; since we'll be blasting fewer words in X
    sub.w #1,blsp_width_words_after_clip  ; adjust

    ; For each 16-pixel-block, we need add 8 to source pointer...
    lsl.w #1,d7  ; ...so since we multiplied d7 by 16, if we now halve d7... you get it.
    add.w d7,a0
    add.w d7,blsp_bl_source_y_inc  ; we also have to adjust source_y_inc for the new width

    lsl.w #2,d7
    add.w d7,blsp_bl_dest_y_inc  ; ...and we adjust dest_y_inc
    
    ; Adjust screen offset to clipping
    move.w #SCREEN_CLIP_X_MIN,d2
    lsr.w #4,d2
    lsl.w #BPL_SHIFTER,d2  ; instead of mulu #2*SCREEN_BITPLANES,d4
    add.w d2,a1

  .no_x_min_clip:

  ;-- Partial clipping
  ;------------------------------------------------------------
  ;-- Y position

  ; Adjust screen pointer - this multiplication should be LUT:ed, obviously
  mulu #SCREEN_WIDTH_BYTES,d1
  add.w d1,a1

  ;-- Y position
  ;------------------------------------------------------------
  ;-- Destination Y increment

  move.w blsp_width_words_after_clip,d7
  sub.w #1,d7  ; because we need one less for dest y inc
  move.w #SCREEN_WIDTH_BYTES,d6
  ; multiply width-in-words to account for words and bitplanes
  lsl.w #BPL_SHIFTER,d7  ; instead of mulu #2*SCREEN_BITPLANES,d7
  sub.w d7,d6  ; subtract total sprite width from total screen width...
  move.w d6,blsp_bl_dest_y_inc  ; ...and that's how much the blaster needs to add each line

  ;-- Destination Y increment
  ;------------------------------------------------------------
  ;-- Skewing and endmasks

  tst.b blsp_skew_value  ; if skew value is 0, that's one codepath...
  bne .skewing  ; if not, that's another.
    ; no skewing
    move.w #-1,blsp_bl_endmask_0  ; left endmask
    move.w #-1,blsp_bl_endmask_2   ; right endmask
    bra .skewing_done
  .skewing:
    ; Fetch endmasks from LUTs
    clr.l d7
    move.b blsp_skew_value,d7
    add.l d7,d7
    lea blsp_leftmasks,a6
    move.w (a6,d7),blsp_bl_endmask_0
    lea blsp_rightmasks,a6
    move.w (a6,d7),blsp_bl_endmask_2

    ; ... and because we've skewed (effectively adding 16 pixels to the right), we need to adapt these values:
    add.w #1,blsp_width_words_after_clip   ; one more xcount
    sub.w #SCREEN_BITPLANES*2,blsp_bl_dest_y_inc  ; adjust dest y inc accordingly
    sub.w #2,blsp_bl_source_y_inc   ; source y inc
  .skewing_done:

  ;-- Skewing and endmasks
  ;------------------------------------------------------------
  ;-- X clipping adjustments

  tst.b bslp_clip_x_max_flag
  beq.s .no_x_max_clip_adjustments
    move.w #-1,blsp_bl_endmask_2  ; since we're clipping on the right, we want the corresponding endmask to show everything
  .no_x_max_clip_adjustments:

  tst.b bslp_clip_x_min_flag
  beq.s .no_x_min_clip_adjustments
    move.w #-1,blsp_bl_endmask_0  ; clipping on the left = leftmost endmask all 1's
    tst.b blsp_skew_value
    bne.s .no_skew_fix
      add.w #1,blsp_width_words_after_clip
      sub.w #SCREEN_BITPLANES*2,blsp_bl_dest_y_inc
      bra.s .skew_fixing_done
    .no_skew_fix:
      move.b #1,blsp_bl_extra_source_read_flag
      add.w #2,blsp_bl_source_y_inc
    .skew_fixing_done:

    cmp.w #1,blsp_width_words_after_clip
    bne .nopers
      ; Since width is a single word, endmask_2 --> endmask_0
      move.w blsp_bl_endmask_2,blsp_bl_endmask_0
      tst.b blsp_skew_value
      beq .dont_change_src_y_inc
        sub #2,blsp_bl_source_y_inc
        sub.w #2,a0
      .dont_change_src_y_inc:
    .nopers:

  .no_x_min_clip_adjustments:


  tst.b blsp_bl_extra_source_read_flag
  beq .no__extra_src_read
    add.b #$c0,blsp_skew_value  ; set NFSR and FXSR bits
  .no__extra_src_read:

  ;-- X clipping adjustments
  ;------------------------------------------------------------
  ;-- Pointers to mask and sprite data

  ; a0 contains the address to the mask, let's save it
  move.l a0,blsp_mask_pointer
  move.w blsp_width_words_before_clip,d7  ; d7 now contains the width of the mask, in words
  move.w blsp_height_pixels_before_clip,d6  ; d6 now contains the height in pixels
  mulu.w d7,d6  ; should be LUT:ed, or why not part of the sprite struct
  add.w d6,d6  ; we double d6 to convert a word-offset to bytes
  add.w d6,a0  ; a0 now points to sprite data
  move.l a0,blsp_sprite_pointer
  ext.l d6
  move.l d6,blsp_bitplane_offset  ; save offset for sprites with multiple bitplanes

  ;-- Pointers to mask and sprite data
  ;------------------------------------------------------------
  ;-- Blast mask

  ; Clear blaster halftone RAM
	move.l #$ffff8a00,a6
	rept 16/2
	  clr.l (a6)+
	endr

  move.l a1,-(sp)  ; save a1 for next bitplane

  move.w blsp_sprite_bitplanes,d7
  subq #1,d7
  ext.l d7
  .blast_mask_loop:
    move.w blsp_bl_source_x_inc,$ffff8a20   ;source x inc
    move.w blsp_bl_source_y_inc,$ffff8a22   ;source y inc
    move.l blsp_mask_pointer,$ffff8a24   ;source address
    move.w blsp_bl_endmask_0,$ffff8a28   ;endmask 0
    move.w blsp_bl_endmask_1,$ffff8a2a  ;endmask 1
    move.w blsp_bl_endmask_2,$ffff8a2c   ;endmask 2
    move.w blsp_bl_dest_x_inc,$ffff8a2e   ;dest x inc
    move.w blsp_bl_dest_y_inc,$ffff8a30   ;dest y inc
    move.l a1,$ffff8a32   ;destination address
    move.w blsp_width_words_after_clip,$ffff8a36   ;x count (n words per line to copy)
    move.w blsp_height_pixels_after_clip,$ffff8a38   ;y count (n lines to copy)
    move.b blsp_skew_value,$ffff8a3d  ; set skew
    move.b #BLASTER_HOP_SOURCE,$ffff8a3a              ; halftone operation
    move.b #BLASTER_OP_SOURCE_AND_TARGET,$ffff8a3b    ; operation
      ;move.b #BLASTER_OP_SOURCE,$ffff8a3b    ; operation
    move.b #BLASTER_COMMAND_START_HOG_MODE,$ffff8a3c  ; start blaster
    
    addq #2,a1  ; offset to next bitplane
  dbra d7,.blast_mask_loop

  move.l (sp)+,a1

  ;-- Blast mask
  ;------------------------------------------------------------
  ;-- Blast sprite

  move.l blsp_bitplane_offset,d6
  move.w blsp_sprite_bitplanes,d7
  subq #1,d7
  ext.l d7
  .blast_sprite_loop:
    move.w blsp_bl_source_x_inc,$ffff8a20   ;source x inc
    move.w blsp_bl_source_y_inc,$ffff8a22   ;source y inc
    move.l blsp_sprite_pointer,$ffff8a24   ;source address
    move.w blsp_bl_endmask_0,$ffff8a28   ;endmask 0
    move.w blsp_bl_endmask_1,$ffff8a2a  ;endmask 1
    move.w blsp_bl_endmask_2,$ffff8a2c   ;endmask 2
    move.w blsp_bl_dest_x_inc,$ffff8a2e   ;dest x inc
    move.w blsp_bl_dest_y_inc,$ffff8a30   ;dest y inc
    move.l a1,$ffff8a32   ;destination address
    move.w blsp_width_words_after_clip,$ffff8a36   ;x count (n words per line to copy)
    move.w blsp_height_pixels_after_clip,$ffff8a38   ;y count (n lines to copy)
    move.b blsp_skew_value,$ffff8a3d  ; set skew
    move.b #BLASTER_HOP_SOURCE,$ffff8a3a              ; halftone operation
    move.b #BLASTER_OP_SOURCE_OR_TARGET,$ffff8a3b    ; operation
      ;move.b #BLASTER_OP_SOURCE_XOR_TARGET,$ffff8a3b    ; operation
      ;move.b #BLASTER_OP_SOURCE,$ffff8a3b    ; operation
    move.b #BLASTER_COMMAND_START_HOG_MODE,$ffff8a3c  ; start blaster

    add.l d6,blsp_sprite_pointer  ; to get to the next sprite bitplane
    addq #2,a1  ; offset to next bitplane
  dbra d7,.blast_sprite_loop

  ;-- Blast sprite
  ;------------------------------------------------------------

.early_out:
  rts


;------------------------------------------------------------------------------


blsp_draw_noclip:
; In:  d0.w - X
;      d1.w - Y
;      a0.l - pointer to sprite struct
;      a1.l - pointer to screen

  clr.b bslp_clip_x_min_flag
  clr.b bslp_clip_x_max_flag
  
  clr.b blsp_bl_extra_source_read_flag

  ;------------------------------------------------------------
  ;-- Setup blaster registers

  ; These are all default values
  move.w #2,blsp_bl_source_x_inc
  move.w #2,blsp_bl_source_y_inc
  move.w #-1,blsp_bl_endmask_1
  move.w #SCREEN_BITPLANES*2,blsp_bl_dest_x_inc

  ;-- Setup blaster registers
  ;------------------------------------------------------------
  ;-- Get width and height of sprite

  ; Get number of bitplanes in sprite
  move.w (a0)+,blsp_sprite_bitplanes
  ; Get sprite width
  move.w (a0)+,d7  ; let's not modify d7 for a while, it's going to see use later
  ; Get sprite height
  move.w (a0)+,d6  ; let's not touch a0 or d6, we're going to use them in a bit
  move.w d6,blsp_height_pixels

  ;-- Get width and height of sprite
  ;------------------------------------------------------------
  ;-- "Early out" clipping

  ; X min
  cmp.w #SCREEN_CLIP_X_MIN,d0
  blt .early_out
  ; Y min
  cmp.w #SCREEN_CLIP_Y_MIN,d1
  blt .early_out
  ;X max
  move.w #SCREEN_CLIP_X_MAX,d2
  sub.w d7,d2
  add.w #1,d2
  cmp.w d2,d0
  bgt .early_out
  ;Y max
  move.w #SCREEN_CLIP_Y_MAX,d2
  sub.w d6,d2
  add.w #1,d2
  cmp.w d2,d1
  bgt .early_out

  ;-- "Early out" clipping
  ;------------------------------------------------------------
  ;-- Get width-in-words

  move.w d7,blsp_width_pixels
  lsr.w #4,d7
  move.w d7,blsp_width_words

  ;-- Get width-in-words
  ;------------------------------------------------------------
  ;-- X position

  move.w d0,d5  ; Back up X so we can still access the untouched value later
  and.b #$f,d5  ; Get lowest 4 bits from X for skewing
  move.b d5,blsp_skew_value
  ; Mask out so we get a clean multiple of 16
  
  move.w d0,d5
  sub.w #SCREEN_CLIP_X_MIN,d5
  tst.w d5
  bmi .x_position_done
    move.w d0,d4
    ; Adjust screen pointer to correct "16-pixel block"
    lsr.w #4,d4
    lsl.w #BPL_SHIFTER,d4  ; instead of mulu #2*SCREEN_BITPLANES,d4
    add.w d4,a1
  .x_position_done:

  ;-- X position
  ;------------------------------------------------------------
  ;-- Y position

  ; Adjust screen pointer - this multiplication should be LUT:ed, obviously
  mulu #SCREEN_WIDTH_BYTES,d1
  add.w d1,a1

  ;-- Y position
  ;------------------------------------------------------------
  ;-- Destination Y increment

  move.w blsp_width_words,d7
  sub.w #1,d7  ; because we need one less for dest y inc
  move.w #SCREEN_WIDTH_BYTES,d6
  ; multiply width-in-words to account for words and bitplanes
  lsl.w #BPL_SHIFTER,d7  ; instead of mulu #2*SCREEN_BITPLANES,d7
  sub.w d7,d6  ; subtract total sprite width from total screen width...
  move.w d6,blsp_bl_dest_y_inc  ; ...and that's how much the blaster needs to add each line

  ;-- Destination Y increment
  ;------------------------------------------------------------
  ;-- Pointers to mask and sprite data

  ; a0 contains the address to the mask, let's save it
  move.l a0,blsp_mask_pointer
  move.w blsp_width_words,d7  ; d7 now contains the width of the mask, in words
  move.w blsp_height_pixels,d6  ; d6 now contains the height in pixels
  mulu.w d7,d6  ; should be LUT:ed, or why not part of the sprite struct
  add.w d6,d6  ; we double d6 to convert a word-offset to bytes
  add.w d6,a0  ; a0 now points to sprite data
  move.l a0,blsp_sprite_pointer
  ext.l d6
  move.l d6,blsp_bitplane_offset  ; save offset for sprites with multiple bitplanes

  ;-- Pointers to mask and sprite data
  ;------------------------------------------------------------
  ;-- Skewing and endmasks

  tst.b blsp_skew_value  ; if skew value is 0, that's one codepath...
  bne .skewing  ; if not, that's another.
    ; no skewing
    move.w #-1,blsp_bl_endmask_0  ; left endmask
    move.w #-1,blsp_bl_endmask_2   ; right endmask
    bra .skewing_done
  .skewing:
    ; Fetch endmasks from LUTs
    clr.l d7
    move.b blsp_skew_value,d7
    add.l d7,d7
    lea blsp_leftmasks,a6
    move.w (a6,d7),blsp_bl_endmask_0
    lea blsp_rightmasks,a6
    move.w (a6,d7),blsp_bl_endmask_2

    ; ... and because we've skewed (effectively adding 16 pixels to the right), we need to adapt these values:
    add.w #1,blsp_width_words  ; one more xcount
    sub.w #SCREEN_BITPLANES*2,blsp_bl_dest_y_inc  ; adjust dest y inc accordingly
    sub.w #2,blsp_bl_source_y_inc   ; source y inc
  .skewing_done:

  ;-- Skewing and endmasks
  ;------------------------------------------------------------
  ;-- Blast mask

  ; Clear blaster halftone RAM
	move.l #$ffff8a00,a6
	rept 16/2
	  clr.l (a6)+
	endr

  move.l a1,-(sp)  ; save a1 for next bitplane

  move.w blsp_sprite_bitplanes,d7
  subq #1,d7
  ext.l d7
  .blast_mask_loop:
    move.w blsp_bl_source_x_inc,$ffff8a20   ;source x inc
    move.w blsp_bl_source_y_inc,$ffff8a22   ;source y inc
    move.l blsp_mask_pointer,$ffff8a24   ;source address
    move.w blsp_bl_endmask_0,$ffff8a28   ;endmask 0
    move.w blsp_bl_endmask_1,$ffff8a2a  ;endmask 1
    move.w blsp_bl_endmask_2,$ffff8a2c   ;endmask 2
    move.w blsp_bl_dest_x_inc,$ffff8a2e   ;dest x inc
    move.w blsp_bl_dest_y_inc,$ffff8a30   ;dest y inc
    move.l a1,$ffff8a32   ;destination address
    move.w blsp_width_words,$ffff8a36   ;x count (n words per line to copy)
    move.w blsp_height_pixels,$ffff8a38   ;y count (n lines to copy)
    move.b blsp_skew_value,$ffff8a3d  ; set skew
    move.b #BLASTER_HOP_SOURCE,$ffff8a3a              ; halftone operation
    move.b #BLASTER_OP_SOURCE_AND_TARGET,$ffff8a3b    ; operation
      ;move.b #BLASTER_OP_SOURCE,$ffff8a3b    ; operation
    move.b #BLASTER_COMMAND_START_HOG_MODE,$ffff8a3c  ; start blaster
    
    addq #2,a1  ; offset to next bitplane
  dbra d7,.blast_mask_loop

  move.l (sp)+,a1

  ;-- Blast mask
  ;------------------------------------------------------------
  ;-- Blast sprite

  move.l blsp_bitplane_offset,d6
  move.w blsp_sprite_bitplanes,d7
  subq #1,d7
  ext.l d7
  .blast_sprite_loop:
    move.w blsp_bl_source_x_inc,$ffff8a20   ;source x inc
    move.w blsp_bl_source_y_inc,$ffff8a22   ;source y inc
    move.l blsp_sprite_pointer,$ffff8a24   ;source address
    move.w blsp_bl_endmask_0,$ffff8a28   ;endmask 0
    move.w blsp_bl_endmask_1,$ffff8a2a  ;endmask 1
    move.w blsp_bl_endmask_2,$ffff8a2c   ;endmask 2
    move.w blsp_bl_dest_x_inc,$ffff8a2e   ;dest x inc
    move.w blsp_bl_dest_y_inc,$ffff8a30   ;dest y inc
    move.l a1,$ffff8a32   ;destination address
    move.w blsp_width_words,$ffff8a36   ;x count (n words per line to copy)
    move.w blsp_height_pixels,$ffff8a38   ;y count (n lines to copy)
    move.b blsp_skew_value,$ffff8a3d  ; set skew
    move.b #BLASTER_HOP_SOURCE,$ffff8a3a              ; halftone operation
    move.b #BLASTER_OP_SOURCE_OR_TARGET,$ffff8a3b    ; operation
      ;move.b #BLASTER_OP_SOURCE_XOR_TARGET,$ffff8a3b    ; operation
      ;move.b #BLASTER_OP_SOURCE,$ffff8a3b    ; operation
    move.b #BLASTER_COMMAND_START_HOG_MODE,$ffff8a3c  ; start blaster

    add.l d6,blsp_sprite_pointer  ; to get to the next sprite bitplane
    addq #2,a1  ; offset to next bitplane
  dbra d7,.blast_sprite_loop

  ;-- Blast sprite
  ;------------------------------------------------------------

.early_out:
  rts


;------------------------------------------------------------------------------


  .bss
blsp_sprite_bitplanes:            ds.w 1

; Address to mask
blsp_mask_pointer:                ds.l 1
; Address to first bitplane in sprite
blsp_sprite_pointer:              ds.l 1
; Offset to next bitplane in sprite
blsp_bitplane_offset:             ds.l 1

; Width and height variables
blsp_width_pixels:      ds.w 1
blsp_height_pixels:     ds.w 1
blsp_width_words:       ds.w 1

; Width and height variables for clipping version
blsp_width_pixels_before_clip:    ds.w 1
blsp_width_pixels_after_clip:     ds.w 1
blsp_width_words_before_clip:     ds.w 1
blsp_width_words_after_clip:      ds.w 1
blsp_height_pixels_before_clip:   ds.w 1
blsp_height_pixels_after_clip:    ds.w 1

; Flags to determine if X clipping is going on
bslp_clip_x_min_flag:             ds.b 1
bslp_clip_x_max_flag:             ds.b 1

; Flag for extra source read
blsp_bl_extra_source_read_flag:   ds.b 1

blsp_skew_value:                  ds.b 1  

; Blaster shadow variables
blsp_bl_source_x_inc:   ds.w 1
blsp_bl_source_y_inc:   ds.w 1
blsp_bl_endmask_0:      ds.w 1
blsp_bl_endmask_1:      ds.w 1
blsp_bl_endmask_2:      ds.w 1
blsp_bl_dest_x_inc:     ds.w 1
blsp_bl_dest_y_inc:     ds.w 1


  .68000


;------------------------------------------------------------------------------


  .data

bfly_palette:
  include "bfly_160x164_4bpl.pal"

bfly_016:
  dc.w 1  ; bitplanes
  dc.w 16, 17  ; width, height
  incbin "bflymask_016x017.1bp"
  incbin "bfly_016x017.1bp"

bfly_032:
  dc.w 1  ; bitplanes
  dc.w 32, 33  ; width, height
  incbin "bflymask_032x033.1bp"
  incbin "bfly_032x033.1bp"

bfly_048:
  dc.w 1  ; bitplanes
  dc.w 48, 50  ; width, height
  incbin "bflymask_048x050.1bp"
  incbin "bfly_048x050.1bp"

bfly_064:
  dc.w 1  ; bitplanes
  dc.w 64, 66  ; width, height
  incbin "bflymask_064x066.1bp"
  incbin "bfly_064x066.1bp"

bfly_080:
  dc.w 1  ; bitplanes
  dc.w 80, 84  ; width, height
  incbin "bflymask_080x084.1bp"
  incbin "bfly_080x084.1bp"

bfly_096:
  dc.w 1  ; bitplanes
  dc.w 96, 99  ; width, height
  incbin "bflymask_096x099.1bp"
  incbin "bfly_096x099.1bp"

bfly_112:
  dc.w 1  ; bitplanes
  dc.w 112, 116  ; width, height
  incbin "bflymask_112x116.1bp"
  incbin "bfly_112x116.1bp"

bfly_128:
  dc.w 1  ; bitplanes
  dc.w 128, 132  ; width, height
  incbin "bflymask_128x132.1bp"
  incbin "bfly_128x132.1bp"

bfly_144:
  dc.w 1  ; bitplanes
  dc.w 144, 149  ; width, height
  incbin "bflymask_144x149.1bp"
  incbin "bfly_144x149.1bp"

bfly_160:
  dc.w 1  ; bitplanes
  dc.w 160, 164  ; width, height
  incbin "bflymask_160x164.1bp"
  incbin "bfly_160x164.1bp"

bfly_160_4bpl:
  dc.w 4  ; bitplanes
  dc.w 160, 164  ; width, height
  incbin "bflymask_160x164.1bp"
  incbin "bfly_160x164_4bpl.4bp"


blsp_leftmasks:
    dc.w %1111111111111111
    dc.w %0111111111111111
    dc.w %0011111111111111
    dc.w %0001111111111111
    dc.w %0000111111111111
    dc.w %0000011111111111
    dc.w %0000001111111111
    dc.w %0000000111111111
    dc.w %0000000011111111
    dc.w %0000000001111111
    dc.w %0000000000111111
    dc.w %0000000000011111
    dc.w %0000000000001111
    dc.w %0000000000000111
    dc.w %0000000000000011
    dc.w %0000000000000001


blsp_rightmasks:
    dc.w %0000000000000000
    dc.w %1000000000000000
    dc.w %1100000000000000
    dc.w %1110000000000000
    dc.w %1111000000000000
    dc.w %1111100000000000
    dc.w %1111110000000000
    dc.w %1111111000000000
    dc.w %1111111100000000
    dc.w %1111111110000000
    dc.w %1111111111000000
    dc.w %1111111111100000
    dc.w %1111111111110000
    dc.w %1111111111111000
    dc.w %1111111111111100
    dc.w %1111111111111110

  .68000


;// Blaster sprite
;//////////////////////////////////////////////////////////////////////////////////////////////
;// ICE


if ICE_ENABLE

  deice:
  ; In: a0 - address of packed data
  ;     a1 - address to unpack to
  	include "_inc\\ice.s"

endif  ; if ICE_ENABLE


;// ICE
;//////////////////////////////////////////////////////////////////////////////////////////////
;// ARJ mode 7


if ARJ_ENABLE

  dearj:
  ; In: a0 - address of packed data
  ;     a1 - address to unpack to
  	include "_inc\\arjmode7.s"

  .bss
  arjbuffer:
  	ds.b 11312
  .68000

endif  ; if ARJ_ENABLE


;// ARJ mode 7
;//////////////////////////////////////////////////////////////////////////////////////////////
;// lZ77


if LZ77_ENABLE

  ;lz77 v1.3 by Ray /TSCC
  ; In:  a0.l: source address
  ;      a1.l: dest address
  
  ;**********************************************
  ;*
  ;*  void d_lz77(a0.l *lz77data, a1.l *dest)
  ;*
  ;* Very! fast lz77 decompression routine
  ;* 68000 version
  ;*
  ;**********************************************
  
  		;section text
  d_lz77:
    addq.l #4,a0		; Skip original length
    bra.s	.loadtag
  
  .literal:
    rept 8
      move.b (a0)+,(a1)+     ; Copy 8 bytes literal string
    endr
           
  .loadtag:
    move.b (a0)+,d0	; Load compression TAG
    beq.s .literal	; 8 bytes literal string?
  
    moveq.l #8-1,d1         ; Process TAG per byte/string
  .search:
    add.b d0,d0		; TAG <<= 1
    bcs.s .compressed
    
    move.b (a0)+,(a1)+     ; Copy another literal byte
    dbra d1,.search
    
    bra.s .loadtag
  
  .compressed:
    moveq.l #0,d2
    move.b (a0)+,d2        ; Load compression specifier
    beq.s .break		; End of stream, exit
    
    moveq.l #$0f,d3		; Mask out stringlength
    and.l d2,d3
    
    lsl.w #4,d2		; Compute string location
    move.b (a0)+,d2
    movea.l a1,a2
    suba.l d2,a2
    
    add.w d3,d3		; Jump into unrolled string copy loop
    neg.w d3
    jmp .unroll(pc,d3.w)
    
    rept 15
      move.b (a2)+,(a1)+
    endr
  .unroll:
    move.b (a2)+,(a1)+
    move.b (a2)+,(a1)+
    
    dbra d1,.search
    
    bra.s .loadtag
  
  .break:
    rts

endif  ; if LZ77_ENABLE


;// lZ77
;//////////////////////////////////////////////////////////////////////////////////////////////
;// Printval - converting num to string


if PRINTVAL_ENABLE

  printval_d0_dec:
    bsr printval_makeval_dec_tass
    lea printval_valuebuffer,a1
    bsr printval_remove_leading_zeroes
    mSysprintA1 
    rts


  ;----------------------------------------------------------------------------  

  
  printval_d0_hex:
    bsr printval_makeval_hex
    lea printval_valuebuffer,a1
    bsr printval_remove_leading_zeroes
    mSysprintA1 
    rts
  
  
  ;----------------------------------------------------------------------------  


  printval_remove_leading_zeroes:
  ; In:  a1.l - pointer to string with numbers
  ; Out: a1.l - same pointer, adjusted
    .loop:
      cmp.b #"0",(a1)
      bne .done
      add.w #1,a1
    bra .loop
  .done:
    rts
  
  
  ;----------------------------------------------------------------------------  


  printval_makeval_dec_tass:
  ; Originally from Turbo Assembler
  
  ; In:  d0.l - value to convert to decimal
  ; Out: printval_valuebuffer - null-terminated string with output
  ;               move.l  #12345,D1       ;number to output
  ;                moveq   #5-1,D4         ;no of digits-1
  ;                lea     out,A0          ;buffer
    move.l d0,d1
    move.l #9,d4
    lea printval_valuebuffer,a0
  ;************************************************************************
  ;* Dezimal-Zahl in D1 ausgeben                                          *
  ;* Anzahl der Stellen in D4                                             *
  ;************************************************************************
  ;                PART 'dezw_out'
  dezw_out:
    movem.l D0-D5/A3,-(SP)
    lea dez_tab(PC),A3
    move.w  D4,D5
    lsl.w   #2,D5
    lea     4(A3,D5.w),A3
    moveq   #'0',D5
  dez_loop:
    move.l  -(A3),D3
    moveq   #$D0,D2
  subtr:
    sub.l   D3,D1
    dbcs    D2,subtr
    neg.b   D2
    move.b  D2,D0
    cmp.b   #'0',D0
    beq.s   dez_zero
    moveq   #'0',D5
  dez_zero2:                              ;bsr     chrout
    move.b  D0,(A0)+
    add.l   D3,D1
    dbra    D4,dez_loop
    move.b #0,(a0)+
    movem.l (SP)+,D0-D5/A3
    rts
  dez_zero:
    move.w  D5,D0
    tst.w   D4
    bne.s   dez_zero2
    moveq   #'0',D0
    bra.s   dez_zero2
  dez_tab:
    DC.L 1,10,100,1000,10000,100000
    DC.L 1000000,10000000,100000000,1000000000
  ;                ENDPART
  ;out:            DS.B 16
  
  
  ;----------------------------------------------------------------------------  


  printval_makeval_dec:
  ; In:  d0.l - value to convert to decimal
  ; Out: printval_valuebuffer - null-terminated string with output
  ; IMPORTANT: this only works for d0 in range 0-34463 (Why it isn't 0-32767 is a mystery at this time)
    lea printval_valuebuffer,a6
    lea printval_pow10,a5
    move.l #0,d2 ; m
  
    move.l #PRINTVAL_VALUEBUFFER_SIZE-1+1,d7
    .loop:
      move.l (a5)+,d6
      move.l d0,d1 ; d1=n
      sub.l d2,d1 ; d1=n-m
      divu d6,d1 ; d1 is value
      ext.l d1
      move.l d1,d5
      add.b #48,d5
      move.b d5,(a6)+
      mulu d6,d1
      add.l d1,d2
    dbra d7,.loop
    move.b #0,(a6)+
    rts
  
  
  ;----------------------------------------------------------------------------  


  printval_makeval_hex:
  ; In:  d0.l - value to convert to decimal
  ; Out: printval_valuebuffer - null-terminated string with output
    lea printval_valuebuffer,a6
    lea printval_hextable,a5
    move.l #8-1,d7
    .loop:
      move.l d0,d1
      move.l (a5)+,d2
      and.l d2,d1
      move.l (a5)+,d3
      beq .noshifting
      sub.l #1,d3
      .shiftloop:
        asr.l #4,d1
      dbra d3,.shiftloop
    .noshifting:
      add.b #48,d1
      cmp.b #"9",d1
      ble .nofixforchars
      add.b #39,d1
    .nofixforchars:
      move.b d1,(a6)+
    dbra d7,.loop
    move.b #0,(a6)+
    rts
  
  
  ;----------------------------------------------------------------------------  


  printval_init::
    lea printval_valuebuffer,a0
    move.l #"0",d6
    move.l #0,d6
    move.l #PRINTVAL_VALUEBUFFER_SIZE_MAX-1,d7
    .loop:
      move.b d6,(a0)+
    dbra d7,.loop
    rts
  
  
  ;----------------------------------------------------------------------------  


  printval_hextable:
    dc.l $f0000000, 7
    dc.l $0f000000, 6
    dc.l $00f00000, 5
    dc.l $000f0000, 4
    dc.l $0000f000, 3
    dc.l $00000f00, 2
    dc.l $000000f0, 1
    dc.l $0000000f, 0
  
  
  ;----------------------------------------------------------------------------  


  printval_pow10:
    ; Nasty but fun solution for powers of 10
    z set PRINTVAL_VALUEBUFFER_SIZE
    ;z set 5
    v set 1
    rept z
      v set v*10
    endr
    rept z+1
      dc.l v
      v set v/10
    endr
  
  
  ;----------------------------------------------------------------------------  


  .bss
  printval_valuebuffer:
    ds.b PRINTVAL_VALUEBUFFER_SIZE_MAX+1,0
    even
  .68000

endif


;// Printval - converting num to string
;//////////////////////////////////////////////////////////////////////////////////////////////
;// File loading


if FILE_LOAD_ENABLE

  file_load_loadfile:
  ; In: a0.l - address of zero-terminated filename string
  ;     a1.l - address to load data at
  ; Out: d0 - 0=ok, all other values means error
    mTimercOn
  	move.l #0,d0
  	move.w #0,-(sp)
  	move.l a0,-(sp)
  	move.w #$3d,-(sp)
  	trap #1
  	add.l #8,sp
  	tst.l d0
  	bmi	.fail
  	move.w d0,file_load_handle
  	bra .filefoundgoon
  .fail:
  	move.l #-1,d0  ; Error
  	mTimercOff
  	rts
  
  .filefoundgoon:
  	move.l a1,-(sp)
  	move.l #1024*1024*4,-(sp)
  	move.w file_load_handle,-(sp)
  	move.w #$3f,-(sp)
  	trap #1
  	add.l #12,sp
  	tst.l d0
  	bmi	.fail
  	move.w file_load_handle,-(sp)
  	move.w #$3e,-(sp)
  	trap #1
  	add.l #4,sp
  	move.l #0,d0  ; No error
    mTimercOff
  	rts
  
  
  ;----------------------------------------------------------------------------


  file_load_error:
    mVblWait 1
    mSetPalette allblackpal
  .meow:
    move.w #0,$ffff8240
    move.w #$700,$ffff8240
    bra .meow


  ;----------------------------------------------------------------------------


  .bss
  file_load_handle: ds.w 1
  .68000

endif  ; if FILE_LOAD_ENABLE


;// File loading
;//////////////////////////////////////////////////////////////////////////////////////////////
; // MegaSTE


if MEGASTE_ENABLE

  turn_megaste_features_off:
    if DO_SYSTEM_TEST
      tst.w systemtest_machine_type_mste
      beq .done
  ;	move.l $8,-(sp)
  ;	move.l #ggntrap1,$8
  	clr.b $ffff8e21
  ;ggntrap1
  ;	move.l (sp)+,$8
  .done:
    endif
  	rts

  
  ;----------------------------------------------------------------------------


  turn_megaste_features_on:
    if DO_SYSTEM_TEST
      tst.w systemtest_machine_type_mste
      beq .done
      ;	move.l $8,-(sp)
      ;	move.l #ggntrap2,$8
      	move.b #$ff,$ffff8e21
      ;ggntrap2
      ;	move.l (sp)+,$8
      .done:
    endif
  	rts

endif  ; if MEGASTE_ENABLE


; // MegaSTE
;//////////////////////////////////////////////////////////////////////////////////////////////
;// Propfont


if PROPFONT_ENABLE

  propfont_init:
    bsr propfont_init_fonttempchar
    bsr propfont_build_fontpostable
    bsr propfont_build_fontwidthconvertertable
    bsr propfont_build_fontwidthtable
  
    bsr propfont_reset_cursor
    rts
  
  
  ;----------------------------------------------------------------------------


  propfont_reset_cursor:
    clr.w propfont_x
    clr.w propfont_y
    move.w #0,propfont_x_min
    move.w #319,propfont_x_max
    rts
  
  
  ;----------------------------------------------------------------------------


  propfont_draw_string_with_linebreak:
  ; In: a0.l - pointer to screen
  ;     a1.l - pointer to null-terminated string
  ;     d1.w - X pos
  ;     d2.w - Y pos
    mPush a0
    move.l a1,a0
    bsr propfont_font_linebreak_string
    mPop a0
    bsr propfont_draw_string
    rts
  
  
  ;----------------------------------------------------------------------------


  propfont_font_linebreak_string:
  ; Uses propfont_x_max and propfont_x_min to add in linebreaks in a text string
  ; In: a0.l - pointer to null-terminated string
  ; Out: nothing, this modifies the string in-place.
    mPushAll
    
    move.l #0,d5 ; d5 is line starting position into string
    move.l #0,d4 ; d4 is last space position into string
    move.l #0,d3 ; d3 is current char position
    
    lea propfont_fontwidthtable,a1
    move.l #0,d0
    move.l d0,d1
    move.l d0,d7
    move.w propfont_x_max,d7
    move.w propfont_x_min,d1
    sub.l d1,d7 ; d7 now contains max width
    move.l #0,d6 ; d6 is line width
  .onechar:
    move.l #0,d0
    move.b (a0,d3),d0 ; d0 is char
    cmp.b #0,d0
    if 1=0
      beq .done
    else
      beq .nowidth
    endif
    cmp.b #32,d0
    beq .nowidth_space
    cmp.b #13,d0
    beq .nowidth_linebreak
    sub.l #33,d0
    move.l d0,d1
    add.l d1,d1 ; (a1,d1) now points to char width
    move.w (a1,d1),d1
    bra .widthdone
  .nowidth_linebreak:
    cmp.w d7,d6 ; (d7=max width) if numLineWidth>=numMaxLineWidth: 
    blt .notlower00
    move.b #13,(a0,d4) ; linebreak at numLastSpacePosition
  .notlower00:
    move.l #0,d6
    bra .widthdone
  .nowidth_space:
    move.l #PROPFONT_FONTSPACEWIDTH,d1
    bra .widthdone
  .nowidth:
    move.l #-1,d1
    bra .widthdone
  .widthdone: ; d1 now contains char width, or -1 if no width
  
    cmp.b #0,d0
    bne .notend01
    cmp.w d7,d6 ; (d7=max width) if numLineWidth>=numMaxLineWidth: 
    blt .notlower01
    move.b #13,(a0,d4) ; linebreak at numLastSpacePosition
    bra .done
  .notlower01:
  .notend01:
  
    move.l #0,d2
    cmp.b #32,d0
    bne .notspace02
    move.l #1,d2
  .notspace02:
    cmp.b #13,d0
    bne .notenter02
    move.l #1,d2
  .notenter02:
    cmp.b #0,d2
    beq .done01
    move.l #PROPFONT_FONTSPACEWIDTH,d1 ; space char width
    cmp.w d7,d6
    blt .notlower02
    move.b #13,(a0,d4)
    move.l #0,d6
    move.l d4,d5
    ;find next non-space char
  .finderloop:
    cmp.b #32,(a0,d5)
    bne .finderdone
    add.l #1,d5
    bra .finderloop
  .finderdone:
    move.l d5,d4 ; numLastSpacePos=numLineStartPos
    move.l d5,d3 ; numCharPos=numLineStartPos
    bra .done01
  .notlower02:
    move.l d3,d4 ; numLastSpacePos=numCharPos
  .done01:
  
    add.w #1,d6
    add.w d1,d6 ; add char width to line width
  
    if 1=0
      tst.b d0
      beq .done
    endif
  
  .nextchar:
    add.l #1,d3
    bra .onechar
  
  .done:
    mPopAll
    rts
  
  
  ;----------------------------------------------------------------------------


  propfont_draw_string_one_at_a_time:
  ; In: a0.l - pointer to screen
  ;     a1.l - pointer to null-terminated string
  ;     d1.w - X pos
  ;     d2.w - Y pos
  ; Out: Nothing
  ; Destroys: Nothing
  ; Updates propfont_x
    add.w d1,propfont_x_min
    move.w d2,propfont_y
    move.l #0,d0
    .onechar:
      cmp.b #0,(a1)
      beq .done
      move.b (a1)+,d0
      cmp.b #13,d0
      bne .notenter
      move.w propfont_x_min,propfont_x
      add.w #PROPFONT_FONTHEIGHT,propfont_y
    bra .onechar
  .notenter:
    move.w propfont_x,d1
    move.w propfont_y,d2
    bsr propfont_draw_char
    mVblWait 1
    bra .onechar
  .done:
    rts
  
  
  ;----------------------------------------------------------------------------


  propfont_draw_string:
  ; In: a0.l - pointer to screen
  ;     a1.l - pointer to null-terminated string
  ;     d1.w - X pos
  ;     d2.w - Y pos
  ; Out: Nothing
  ; Destroys: Nothing
  ; Updates propfont_x
    move.l #0,d0
    .onechar:
      cmp.b #0,(a1)
      beq .done
      move.b (a1)+,d0
      cmp.b #13,d0
      bne .notenter
      move.w propfont_x_min,propfont_x
      add.w #PROPFONT_FONTHEIGHT,propfont_y
    bra .onechar
  .notenter:
    bsr propfont_draw_char
    bra .onechar
  .done:
    rts


  ;----------------------------------------------------------------------------


  propfont_draw_char:
  ; In: a0.l - pointer to screen
  ;     d0.b - char
  ;     propfont_x.w - X pos
  ;     propfont_y.w - Y pos
  ; Out: Nothing
  ; Destroys: Nothing
  ; Updates propfont_x
    cmp.b #32,d0
    bne .regularchar
    add.w #1+PROPFONT_FONTSPACEWIDTH,propfont_x
    rts
  .regularchar:
    mPushAll
    lea propfont_font,a2
    lea propfont_fontpostable,a1
    and.l #$ff,d0
    sub.l #33,d0
    
    add.l d0,d0
    add.w (a1,d0),a2 ; a2 points to char in font
  
    ; copy char to temp char
    lea propfont_fonttempchar,a6
    move.l #PROPFONT_FONTHEIGHT-1,d7
    .fontcopy:
      move.l (a2),(a6)+
      add.l #40,a2
    dbra d7,.fontcopy
  
    lea ytable,a1
    move.w propfont_y,d2
    add.w d2,d2
    add.w d2,d2
    add.l (a1,d2.w),a0 ; a0 now points to the correct line
  
    move.w propfont_x,d1
    ext.l d1
    move.l d1,d2
    and.l #$000f,d2 ; d2 is now x "in-word" position (= loop counter)
    and.l #$fffffff0,d1
    lsr.l #4,d1
    lsl.l #3,d1 ; d1 is now offset into line
    add.l d1,a0 ; a0 now point to correct word
    cmp.w #0,d2
    beq .norotate
    ;rotate char
    lea propfont_fonttempchar,a6
    sub.l #1,d2
  .rotatechar:
    mPush a6
    move.l #PROPFONT_FONTHEIGHT,d6
    sub.l #1,d6
    .height_loop1:
      move.w #0,ccr
      roxr.w (a6)+
      roxr.w (a6)+
    dbra d6,.height_loop1
    mPop a6
    dbra d2,.rotatechar
  .norotate:
    ; OR char onto screen
    lea propfont_fonttempchar,a6
    move.l #PROPFONT_FONTHEIGHT,d6
    sub.l #1,d6
    .height_loop2:
      move.w (a6)+,d7
      or.w d7,(a0)
      add.l #8,a0
      move.w (a6)+,d7
      or.w d7,(a0)
      add.l #160-8,a0
    dbra d6,.height_loop2
    lea propfont_fontwidthtable,a1
    move.w (a1,d0),d0
    add.w d0,propfont_x
    add.w #1,propfont_x ; space between chars
    mPopAll
    rts


  ;----------------------------------------------------------------------------


  propfont_build_fontwidthtable:
    mPushAll
    lea propfont_font,a0
    lea propfont_fontpostable,a1
    lea propfont_fontwidthconvertertable,a2
    lea propfont_fontwidthtable,a6
  
    move.l #(PROPFONT_FONTCHARSLINES*PROPFONT_FONTCHARSPERLINE)-1,d7
    .onechar:
      move.w (a1)+,d0
      ext.l d0 ; d0 = offset into a0 - so (a0,d0) is now start of char
      add.l #(PROPFONT_FONTHEIGHT)*40,d0 ; (a0,d0) now points to width in graphics
      lea propfont_fontwidthconvertertable,a2
      move.w #0,d2 ; final width
      move.w (a0,d0),d1
      move.l #PROPFONT_FONTWIDTHCONVERTERTABLESIZE-1,d6
      .convloop:
        cmp.w (a2),d1
        beq .convmatch
        add.l #4,a2
      dbra d6,.convloop
      bra .convgoon ; no match found
    .convmatch:
      move.w 2(a2),d2 ; final width
    .convgoon:
      move.w d2,(a6)+
    dbra d7,.onechar
    move.l #4-1,d7
    .endloop:
      move.l #-1,(a6)+
    dbra d7,.endloop
    mPopAll
    rts
  
  
  ;----------------------------------------------------------------------------


  propfont_build_fontpostable:
  ;o set 0
  ;  rept PROPFONT_FONTCHARSLINES
  ;  dc.w o+0, o+4, o+8, o+12, o+16, o+20, o+24, o+28, o+32, o+36
  ;o set o+((PROPFONT_FONTHEIGHT+1)*40)
  ;  endr
    lea propfont_fontpostable,a0
    clr.l d4
    move.l #PROPFONT_FONTCHARSLINES-1,d7
    .outer:
      move.l d4,d5
      move.l #10-1,d6
      .inner:
        move.w d5,(a0)+
        add.l #4,d5
      dbra d6,.inner
      add.l #((PROPFONT_FONTHEIGHT+1)*40),d4
    dbra d7,.outer
    rts
  
  
  ;----------------------------------------------------------------------------


  propfont_build_fontwidthconvertertable:
  ;  dc.w %0000000000000000, 0
  ;  dc.w %1000000000000000, 1
  ;  dc.w %1100000000000000, 2
  ;  dc.w %1110000000000000, 3
  ;  dc.w %1111000000000000, 4
  ;  dc.w %1111100000000000, 5
  ;  dc.w %1111110000000000, 6
  ;  dc.w %1111111000000000, 7
  ;  dc.w %1111111100000000, 8
  ;  dc.w %1111111110000000, 9
  ;  dc.w %1111111111000000, 10
  ;  dc.w %1111111111100000, 11
  ;  dc.w %1111111111110000, 12
  ;  dc.w %1111111111111000, 13
  ;  dc.w %1111111111111100, 14
  ;  dc.w %1111111111111110, 15
  ;  dc.w %1111111111111111, 16
    lea propfont_fontwidthconvertertable,a0
    clr.l d0
    move.l d0,d1
    move.l #17-1,d7
    .loop:
      move.w d0,(a0)+
      move.w d1,(a0)+
      lsr.l #1,d0
      or.w #%1000000000000000,d0
      add.w #1,d1
    dbra d7,.loop
    rts
  
  
  ;----------------------------------------------------------------------------


  propfont_init_fonttempchar:
    lea propfont_fonttempchar,a0
    move.l #PROPFONT_FONTHEIGHT-1,d7
    .loop:
      clr.l (a0)+
    dbra d7,.loop
    rts


  ;----------------------------------------------------------------------------


  .data
  propfont_font:  
    iif PROPFONT_FONT_SELECT=PROPFONT_FONT_PER02, include "_inc/propfont/propfont_font02.s" ; Per02
    iif PROPFONT_FONT_SELECT=PROPFONT_FONT_PER, include "_inc/propfont/propfont_font03.s" ; Per
    iif PROPFONT_FONT_SELECT=PROPFONT_FONT_MV_BOLI, include "_inc/propfont/propfont_font04.s" ; MV Boli
    iif PROPFONT_FONT_SELECT=PROPFONT_FONT_PALATINO, include "_inc/propfont/propfont_font05.s" ; Palatino Linotype
    iif PROPFONT_FONT_SELECT=PROPFONT_FONT_CALIBRI_LIGHT, include "_inc/propfont/propfont_font06.s" ; Calibri Light
    iif PROPFONT_FONT_SELECT=PROPFONT_FONT_MACINTOSH_1984, include "_inc/propfont/propfont_font07.s" ; Macintosh 1984
    iif PROPFONT_FONT_SELECT=PROPFONT_FONT_ZX_SPECTRUM, include "_inc/propfont/propfont_font08.s" ; ZX Spectrum
    iif PROPFONT_FONT_SELECT=PROPFONT_FONT_MSX, include "_inc/propfont/propfont_font09.s" ; MSX
    iif PROPFONT_FONT_SELECT=PROPFONT_FONT_ATARI_8BIT, include "_inc/propfont/propfont_font10.s" ; Atari 8-bit
  .68000


  ;----------------------------------------------------------------------------


    .bss
  propfont_x:        ds.w 1
  propfont_x_min:    ds.w 1
  propfont_x_max:    ds.w 1
  propfont_y:        ds.w 1
  
  propfont_fontpostable:
    ds.w PROPFONT_FONTCHARSLINES*10
  
  propfont_fontwidthtable:
    ds.w PROPFONT_FONTCHARSLINES*PROPFONT_FONTCHARSPERLINE
  
  propfont_fontwidthconvertertable:
    ds.w 17*2
  
  propfont_fonttempchar:
    ds.l PROPFONT_FONTHEIGHT
    .68000

endif  ; if PROPFONT_ENABLE


;// Propfont
;//////////////////////////////////////////////////////////////////////////////////////////////
;// System test


if DO_SYSTEM_TEST

  systemtest:
  
  ;-- from DHS:
  ; Atari ST/e synclock demosystem
  ; September 2, 2011
  ;
  ; sys/cookie.s
  
  maxcookie	equ	128				;Maximum cookie entries to search
  
  
  ;cookie_check:
  		move.l	$5a0.w,d0
  		beq.s	.st				;Null pointer = ST
  		move.l	d0,a0
  
  		moveq	#maxcookie-1,d7
  .search_mch:
    	tst.l	(a0)
  		beq.s	.st				;Null termination of cookiejar, no _MCH found = ST
  
  		cmp.l	#"_MCH",(a0)
  		beq.s	.mch_found
  		addq.l	#8,a0
  		dbra	d7,.search_mch
  		bra.s	.st				;Default to ST
  
  .mch_found:
    	move.l	4(a0),d0
  		cmp.l	#$00010000,d0
  		beq.s	.ste
  		cmp.l	#$00010010,d0
  		beq.s	.megaste
  		cmp.l	#$00020000,d0
  		beq.s	.tt
  		cmp.l	#$00030000,d0
  		beq.s	.falcon
  
  .st:
    move.l	#"ST  ",systemtest_computer_type
    move.w #1,systemtest_machine_type_st
  		bra.s	.cookie_done
  
  .ste:
  	move.l	#"STe ",systemtest_computer_type
    move.w #1,systemtest_machine_type_ste
  		bra.s	.cookie_done
  
  .megaste:
  	move.l	#"MSTe",systemtest_computer_type
    move.w #1,systemtest_machine_type_mste
  		bra.s	.cookie_done
  
  .tt:
  	move.l	#"TT  ",systemtest_computer_type
    move.w #1,systemtest_machine_type_tt
  		bra.s	.cookie_done
  
  
  .falcon:
    	;Check if we are on CT60/3
  		move.l	$5a0.w,a0
  		moveq	#maxcookie-1,d7
  .search_ct60:
  		cmp.l	#"CT60",(a0)
  		beq.s	.f060
  		addq.l	#8,a0
  		dbra	d7,.search_ct60
  
  .f030:
  	move.l	#"F030",systemtest_computer_type
    move.w #-1,systemtest_machine_type_f030
  		bra.s	.cookie_done
  .f060:
  	move.l	#"F060",systemtest_computer_type
    move.w #-1,systemtest_machine_type_f060
  
  .cookie_done:
  
  
    ; RAM
    move.l $42e,d0
    ; $0008 0000 - 0.5 MB
    ; $0010 0000 - 1 MB
    ; $0020 0000 - 2 MB
    ; $0028 0000 - 2.5 MB
    ; $0040 0000 - 4 MB
    ; $00e0 0000 - 14 MB
    swap d0
  
    cmp.l #$8,d0
    bne .not_0_5
    lea systemtest_string_ram_512k,a6
    bra .ram_detect_done
  .not_0_5:
    cmp.l #$10,d0
    bne .not_1
    lea systemtest_string_ram_1M,a6
    bra .ram_detect_done
  .not_1:
    cmp.l #$20,d0
    bne .not_2
    lea systemtest_string_ram_2M,a6
    bra .ram_detect_done
  .not_2:
    cmp.l #$28,d0
    bne .not_2_5
    lea systemtest_string_ram_2_5M,a6
    bra .ram_detect_done
  .not_2_5:
    cmp.l #$40,d0
    bne .not_4
    lea systemtest_string_ram_4M,a6
    bra .ram_detect_done
  .not_4:
    cmp.l #$e0,d0
    bne .not_14
    lea systemtest_string_ram_14M,a6
    bra .ram_detect_done
  .not_14:
    lea systemtest_string_ram_unknown,a6
  
  .ram_detect_done:
    move.l a6,systemtest_ram_string_pointer
  
    ;TOS version
    move.l 4,d0
    clr.b d0
    move.l d0,a0
    clr.l d0
    clr.l d1
    move.b 2(a0),d0
    move.b 3(a0),d1
    move.l d1,d2
    lea systemtest_tos_string,a6
    clr.l d7
    move.b #"0",d7
    move.l d7,d6
    add.b d0,d6
    move.b d6,0(a6) ;X.xx
    move.l d7,d6
    lsr #4,d1
    add.b d1,d6
    move.b d6,2(a6) ;x.Xx
    move.l d7,d6
    and.b #$f,d2
    add.b d2,d6
    move.b d6,3(a6) ;x.xX
  
    rts


  ;---------------------------------------------------------------------------- 


  systemtest_print:
    mSetPalette systemtest_palette
  
    .iif SPEED_UP_SYSTEM_TEST=0, mVblWait 10
    mSysprint systemtest_string_starting
    .iif SPEED_UP_SYSTEM_TEST=0, mVblWait 25
    mSysprint systemtest_string_cookie
    .iif SPEED_UP_SYSTEM_TEST=0, mVblWait 25
    mSysprint systemtest_string_system_detected
    mSysprint systemtest_computer_type
    .iif SPEED_UP_SYSTEM_TEST=0, mVblWait 15
    mSysprint systemtest_string_ram
    move.l systemtest_ram_string_pointer,a1
    ;bsr sysprint_print_string
    mSysprintA1
    mSysprint systemtest_line_break
    .iif SPEED_UP_SYSTEM_TEST=0, mVblWait 15
  
    mSysprint systemtest_tos_header
    mSysprint systemtest_tos_string
    .iif SPEED_UP_SYSTEM_TEST=0, mVblWait 35
    rts

  
  ;---------------------------------------------------------------------------- 


    .data
  systemtest_computer_type:
  	dcb.b 4," "				;"ST  ", "STe ", "MSTe", "TT  ", "F030", "F060"
    dc.b 13,0
    even
  
  systemtest_string_starting:
    dc.b "Starting XiA demosystem",13
  
    if PROPFONT_ENABLE
      .iif PROPFONT_FONT_SELECT=PROPFONT_FONT_PER02, dc.b "-----------------------",13
      .iif PROPFONT_FONT_SELECT=PROPFONT_FONT_PER, dc.b "----------------------------",13
      .iif PROPFONT_FONT_SELECT=PROPFONT_FONT_MV_BOLI, dc.b "-----------------------------",13
      .iif PROPFONT_FONT_SELECT=PROPFONT_FONT_PALATINO, dc.b "-----------------------------------",13
      .iif PROPFONT_FONT_SELECT=PROPFONT_FONT_CALIBRI_LIGHT, dc.b "----------------------------------",13
      .iif PROPFONT_FONT_SELECT=PROPFONT_FONT_MACINTOSH_1984, dc.b "--------------------------",13
      .iif PROPFONT_FONT_SELECT=PROPFONT_FONT_ZX_SPECTRUM, dc.b "-----------------------",13
      .iif PROPFONT_FONT_SELECT=PROPFONT_FONT_MSX, dc.b "-----------------------",13
      .iif PROPFONT_FONT_SELECT=PROPFONT_FONT_ATARI_8BIT, dc.b "-----------------------",13
    else
      dc.b "------------------------",13
    endif
    dc.b 0
    even
  
  systemtest_string_cookie:
    mPutStringCr "Requesting system cookie..."
  systemtest_string_system_detected:
    mPutString "  Machine: "
  systemtest_string_ram:
    mPutString "  RAM: "
  systemtest_string_ram_512k:
    mPutString "512Kb"
  systemtest_string_ram_1M:
    mPutString "1Mb"
  systemtest_string_ram_2M:
    mPutString "2Mb"
  systemtest_string_ram_2_5M:
    mPutString "2.5Mb"
  systemtest_string_ram_4M:
    mPutString "4Mb"
  systemtest_string_ram_14M:
    mPutString "14Mb"
  systemtest_string_ram_unknown:
    mPutString "? Detection failure"
  systemtest_line_break:
    mPutString 13
  systemtest_tos_header:
    mPutString "  TOS version: "
  systemtest_tos_string:
    mPutStringCr "x.xx"
  .68000


  ;---------------------------------------------------------------------------- 


    .bss
  systemtest_machine_type_st:     ds.w 1
  systemtest_machine_type_ste:    ds.w 1
  systemtest_machine_type_mste:   ds.w 1
  systemtest_machine_type_tt:     ds.w 1
  systemtest_machine_type_f030:   ds.w 1
  systemtest_machine_type_f060:   ds.w 1
  systemtest_ram_string_pointer:  ds.l 1
  systemtest_ram_tosver_major:    ds.b 1
  systemtest_ram_tosver_minor:    ds.b 1
    .68000  

endif  ; if DO_SYSTEM_TEST


;// System test
;//////////////////////////////////////////////////////////////////////////////////////////////
;// Sysprint code


if SYSPRINT_ENABLE

  sysprint_print_string:
  ; In:  a0.l - pointer to start of screen address
  ;      a1.l - address to null-terminated string
  ;      sysprint_x.w - x position for output (0-39)
  ;      sysprint_y.w - y position for output (0-25)
  ;  ifne SYSTEM_TEST_USE_PROPFONT
  ;    bsr propfont_draw_string_with_linebreak
  ;    rts
  ;  endif
  .getchar:
    move.b (a1)+,d0
    beq .alldone ; 0=exit
    cmp.b #13,d0
    bne .nolinebreak
  
    ; linebreak
  .linebreak:
    clr.w sysprint_x
    add.w #1,sysprint_y
    bra .getchar
  .nolinebreak:
  
    ; regular char
    bsr sysprint_print_char
    add.w #1,sysprint_x
    cmp #40,sysprint_x
    bge .linebreak ; force linebreak if x>40
    bra .getchar
    
  .alldone:
    rts
  
  
  ;----------------------------------------------------------------------------


  sysprint_print_char:
  ; In:  d0.b - char
  ;      a0.l - pointer to start of screen address
  ;      sysprint_x.w - x position for output (0-39)
  ;      sysprint_y.w - y position for output (0-25)
    mPush a0
    and.w #$00ff,d0
    lea sysprint_charmap,a5
    move.b (a5,d0.w),d0
  
    sub.b #32,d0 ; font starts at char 32
    
    mulu.w #5,d0
    lea sysprint_font,a6
    
    ; adjust for y
    move.w sysprint_y,d1
  sysprint_line_height equ 9
    mulu.w #160*sysprint_line_height,d1
    add.w d1,a0
    
    ; adjust for x
    move.w sysprint_x,d1
    move.w d1,d2
    and.w #1,d2 ; mask out lowest bit
    ;and.w #$fffe,d1
    lsr.w #1,d1
    lsl.w #3,d1
    add.w d1,a0
    add.w d2,a0
  
    ; draw char  
    move.b (a6,d0.w),(a0)
  lineoffs set 160
  fontoffs set 1
    rept 4
      move.b fontoffs(a6,d0),lineoffs(a0)
      lineoffs set lineoffs+160
      fontoffs set fontoffs+1
    endr
    mPop a0
    rts
  
  
  ;----------------------------------------------------------------------------


  sysprint_init:
    lea sysprint_charmap,a0
    move.l a0,a1
    ; first map everything 1:1
    clr.b d0
    move.w #256-1,d7
    .onechar:
      move.b d0,(a0)+
      add.b #1,d0
    dbra d7,.onechar
  
    ; map back lower case to upper case
    move.b #33+32,d0
    add.w #64+32,a1
    move.b #7+32,(a1)+ ; apostrophe
    move.w #26-1,d7
    .fixlowercase:
      move.b d0,(a1)+
      add.b #1,d0
    dbra d7,.fixlowercase
    rts


  ;----------------------------------------------------------------------------


  .data
  sysprint_font:
    if !PROPFONT_ENABLE
      incbin "sysprint_font.bin" ; 295 bytes
      even
    endif
  .68000


  ;----------------------------------------------------------------------------


  .bss
  sysprint_x:         ds.w 1
  sysprint_y:         ds.w 1
  sysprint_charmap:   ds.b 256
  .68000

endif  ; if SYSPRINT_ENABLE


;// Sysprint code
;//////////////////////////////////////////////////////////////////////////////////////////////
;// Various waiters


wait4vbld0:
; In: d0 - number of vbls to wait-1 (i e prepared for dbra)
; Destroys: d0
  .onevbl:
  	bsr wait4vbl
	dbra d0,.onevbl
	rts


;------------------------------------------------------------------------------


wait4vbl:
	move.w #$1,vblflag
  .loop:
  	cmp.w #0,vblflag
	bne .loop
	rts


;------------------------------------------------------------------------------


waitforfirstline:
	mPush d0
  .sync:
  	move.b $ffff8209.w,d0
	beq.s .sync
	not.b d0
	lsr.b d0,d0
	mPop d0
	rts


;------------------------------------------------------------------------------


waitforvblwaiter:
; In: d0.w - frame number to wait for
	mPush d1
  .wait:
  	move.w vblwaiter,d1
  	cmp.w d0,d1
  	bge .done
  	bsr wait4vbl
	bra .wait
.done:
	mPop d1
	rts


;------------------------------------------------------------------------------


waitforframe:
; In: d0.w - frame number to wait for
	mPush d1
  .wait:
  	move.l framecounter,d1
  	cmp.l d0,d1
  	bge .done
  	bsr wait4vbl
	bra .wait
.done:
	mPop d1
	rts


;// Various waiters
;//////////////////////////////////////////////////////////////////////////////////////////////
;// Palette fading


if PALETTE_FADING_ENABLE

  PALETTE_FADING_USE_MACROS equ 1

; ToDo
; -----
; # Q: Can the register saving in the macros be thrown out?
;       A: Probably not, no

  ;////////////////////////////////////////////////////////////////////////////
  ;// Palette fading macros


  if PALETTE_FADING_USE_MACROS

    macro mFadeColorSplit
      ; In:   d0.w - RGB word (normal)
      ; Out:  d1.w - R
      ;       d2.w - G
      ;       d3.w - B
      ; Destroys: nothing
    	move.w d0,d1
    	move.w d0,d2
    	move.w d0,d3
    	and.w #$f00,d1
    	lsr.w #8,d1
    	and.w #$0f0,d2
    	lsr.w #4,d2
    	and.w #$00f,d3
    endm


    ;--------------------------------------------------------------------------


    macro mFadeColorMerge
      ; In:   d1.w - R
      ;       d2.w - G
      ;       d3.w - B
      ; Out:  d0.w - RGB word (normal)
      ; Destroys: d1, d2, d3
    	lsl.w #8,d1
    	move.w d1,d0
    	lsl.w #4,d2
    	add.w d2,d0
    	add.w d3,d0
    endm


    ;--------------------------------------------------------------------------


    macro mFadeConvertNormalToSte
      ; In:   d0.w - RGB word (normal)
      ; Out:  d0.w - RGB word (STE)
      ; Destroys: nothing
    	mPush d1
    	move.w	d0,d1
    	and.w	#%0000000100010001,d1
    	and.w	#%0000111011101110,d0
    	lsl.w	#3,d1
    	lsr.w	#1,d0
    	add.w	d1,d0
    	mPop d1
    endm

    
    ;--------------------------------------------------------------------------


    macro mFadeConvertSteToNormal
      ; In:   d0.w - RGB word (STE)
      ; Out:  d0.w - RGB word (normal)
      ; Destroys: nothing
    	mPush d1
    	move.w d0,d1
    	and.w	#%0000100010001000,d1
    	and.w	#%0000011101110111,d0
    	lsr.w	#3,d1
    	lsl.w	#1,d0
    	add.w	d1,d0
    	mPop d1
    endm
  	
  endif  ; if PALETTE_FADING_USE_MACROS


  ;// Palette fading macros
  ;////////////////////////////////////////////////////////////////////////////
  ;// Palette fading subroutines


  if PALETTE_FADING_USE_MACROS=0

    fadecolorsplit:
    ; In: d0 - RGB word (normal)
    ; Out: d1 - R
    ;      d2 - G
    ;      d3 - B
    ; Destroys: nothing
    	move.w d0,d1
    	move.w d0,d2
    	move.w d0,d3
    	and.w #$f00,d1
    	lsr.w #8,d1
    	and.w #$0f0,d2
    	lsr.w #4,d2
    	and.w #$00f,d3
    	rts


    ;--------------------------------------------------------------------------


    fadecolormerge:
    ; In: d1 - R
    ;     d2 - G
    ;     d3 - B
    ; Out: d0 - RGB word (normal)
    ; Destroys: d1, d2, d3
    	lsl.l #8,d1
    	move.l d1,d0
    	lsl.l #4,d2
    	add.l d2,d0
    	add.l d3,d0
    	rts


    ;--------------------------------------------------------------------------


    fadeconvertnormal2ste:
    ; In: d0 - RGB word (normal)
    ; Out: d0 - RGB word (STE)
    ; Destroys: nothing
    	mPush d1
    	move.w	d0,d1
    	and.w	#%0000000100010001,d1
    	and.w	#%0000111011101110,d0
    	lsl.w	#3,d1
    	lsr.w	#1,d0
    	add.w	d1,d0
    	mPop d1
    	rts


    ;--------------------------------------------------------------------------


    fadeconvertste2normal:
    ; In: d0 - RGB word (STE)
    ; Out: d0 - RGB word (normal)
    ; Destroys: nothing
    	mPush d1
    	move.w	d0,d1
    	and.w	#%0000100010001000,d1
    	and.w	#%0000011101110111,d0
    	lsr.w	#3,d1
    	lsl.w	#1,d0
    	add.w	d1,d0
    	mPop d1
    	rts

  endif  ; if PALETTE_FADING_USE_MACROS=0


  ;// Palette fading subroutines
  ;////////////////////////////////////////////////////////////////////////////


  ;----------------------------------------------------------------------------


  fadepalettesaesthetic:
  ; In: a0 - address of target palette
  ;     d0 - number of vbls to wait between steps (routine will take this amount * 15 frames)
  	mPush d7
  	bsr fadepalettessetup
  	move.l #16-1,d7
    .onefade:
    	bsr fadepalettesdoonefade
    	mPush d0
    	bsr wait4vbld0
    	mPop d0
  	dbra d7,.onefade
  	mPop d7
  	rts


  ;----------------------------------------------------------------------------


  fadepalettessetup:
  ; In: a0 - address of target palette
  	mPushAll
  	lea currentpalette,a1
  	lea fadecomponents,a2
  	move.l #16-1,d7
    .onecolor:
    	moveq.l #0,d0
    	move.w (a1)+,d0
    	.if PALETTE_FADING_USE_MACROS
      	mFadeConvertSteToNormal
    	  mFadeColorSplit
    	.else
      	bsr fadeconvertste2normal
    	  bsr fadecolorsplit
    	.endif
    	move.w d1,d4 ; R
    	move.w d2,d5 ; G
    	move.w d3,d6 ; B
    	move.w (a0)+,d0
    	.if PALETTE_FADING_USE_MACROS
      	mFadeConvertSteToNormal
    	  mFadeColorSplit
    	.else
      	bsr fadeconvertste2normal
    	  bsr fadecolorsplit
    	.endif
    	; Now we have source RGB in d4/d5/d6 and target RGB in d0/d1/d2
    	; R
    	sub.w d4,d1
    	tst.w d1
    	bmi .Rneg
    .Rpos:
    	lea fadetableadd,a3
    	bra .Rgoon
    .Rneg:
    	lea fadetablesub,a3
    	neg.w d1
    .Rgoon:
    	lsl.w #4,d1
    	add.w d1,a3
    	move.l a3,(a2)+
    	move.w d4,(a2)+
    	; G
    	sub.w d5,d2
    	tst.w d2
    	bmi .Gneg
    .Gpos:
    	lea fadetableadd,a3
    	bra .Ggoon
    .Gneg:
    	lea fadetablesub,a3
    	neg.l d2
    .Ggoon:
    	lsl.w #4,d2
    	add.w d2,a3
    	move.l a3,(a2)+
    	move.w d5,(a2)+
    	; B
    	sub.w d6,d3
    	tst.w d3
    	bmi .Bneg
    .Bpos:
    	lea fadetableadd,a3
    	bra .Bgoon
    .Bneg:
    	lea fadetablesub,a3
    	neg.l d3
    .Bgoon:
    	lsl.w #4,d3
    	add.w d3,a3
    	move.l a3,(a2)+
    	move.w d6,(a2)+
  	dbra d7,.onecolor
  	clr.w fadetablepos
  	mPopAll
  	rts


  ;----------------------------------------------------------------------------


  fadepalettesdoonefade:
  	cmp.w #16,fadetablepos
  	bge .alldone
  	mPushAll
  	lea fadecomponents,a0
  	lea currentpalette,a1
  	move.l #16-1,d7
    .onecomponent:
    	moveq.l #0,d0
    	; R
    	move.l (a0),a2
    	addq.l #1,(a0)
    	addq.l #4,a0
    	move.w (a0)+,d1
    	add.b (a2),d1
    	; G
    	move.l (a0),a2
    	addq.l #1,(a0)
    	addq.l #4,a0
    	move.w (a0)+,d2
    	add.b (a2),d2
    	; B
    	move.l (a0),a2
    	addq.l #1,(a0)
    	addq.l #4,a0
    	move.w (a0)+,d3
    	add.b (a2),d3
    	; Put it all together
    	.if PALETTE_FADING_USE_MACROS
      	mFadeColorMerge
      	mFadeConvertNormalToSte
    	.else
      	bsr fadecolormerge
      	bsr fadeconvertnormal2ste
    	.endif
    	move.w d0,(a1)+
  	dbra d7,.onecomponent
  	addq.w #1,fadetablepos
  	mPopAll
  .alldone:
  	rts

endif  ; if PALETTE_FADING_ENABLE


;------------------------------------------------------------------------------


if PALETTE_FADING_ENABLE

  .data
  fadetableadd: ; 272 bytes
  ; fadetable positive values
   dc.b 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
   dc.b 0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1
   dc.b 0,0,0,1,1,1,1,1,1,1,1,2,2,2,2,2
   dc.b 0,0,1,1,1,1,1,2,2,2,2,2,2,3,3,3
   dc.b 0,1,1,1,1,2,2,2,2,3,3,3,3,4,4,4
   dc.b 0,1,1,1,2,2,2,3,3,3,3,4,4,4,5,5
   dc.b 0,1,1,2,2,2,3,3,3,4,4,5,5,5,6,6
   dc.b 0,1,1,2,2,3,3,4,4,4,5,5,6,6,7,7
   dc.b 1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8
   dc.b 1,1,2,2,3,3,4,5,5,6,6,7,7,8,8,9
   dc.b 1,1,2,3,3,4,4,5,6,6,7,8,8,9,9,10
   dc.b 1,1,2,3,3,4,5,6,6,7,8,8,9,10,10,11
   dc.b 1,2,2,3,4,5,5,6,7,8,8,9,10,11,11,12
   dc.b 1,2,2,3,4,5,6,7,7,8,9,10,11,11,12,13
   dc.b 1,2,3,4,4,5,6,7,8,9,10,11,11,12,13,14
   dc.b 1,2,3,4,5,6,7,8,8,9,10,11,12,13,14,15
   dc.b 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16
  even


  fadetablesub: ; 272 bytes
  ; fadetable negative values
   dc.b -0,-0,-0,-0,-0,-0,-0,-0,-0,-0,-0,-0,-0,-0,-0,0
   dc.b -0,-0,-0,-0,-0,-0,-0,-1,-1,-1,-1,-1,-1,-1,-1,-1
   dc.b -0,-0,-0,-1,-1,-1,-1,-1,-1,-1,-1,-2,-2,-2,-2,-2
   dc.b -0,-0,-1,-1,-1,-1,-1,-2,-2,-2,-2,-2,-2,-3,-3,-3
   dc.b -0,-1,-1,-1,-1,-2,-2,-2,-2,-3,-3,-3,-3,-4,-4,-4
   dc.b -0,-1,-1,-1,-2,-2,-2,-3,-3,-3,-3,-4,-4,-4,-5,-5
   dc.b -0,-1,-1,-2,-2,-2,-3,-3,-3,-4,-4,-5,-5,-5,-6,-6
   dc.b -0,-1,-1,-2,-2,-3,-3,-4,-4,-4,-5,-5,-6,-6,-7,-7
   dc.b -1,-1,-2,-2,-3,-3,-4,-4,-5,-5,-6,-6,-7,-7,-8,-8
   dc.b -1,-1,-2,-2,-3,-3,-4,-5,-5,-6,-6,-7,-7,-8,-8,-9
   dc.b -1,-1,-2,-3,-3,-4,-4,-5,-6,-6,-7,-8,-8,-9,-9,-10
   dc.b -1,-1,-2,-3,-3,-4,-5,-6,-6,-7,-8,-8,-9,-10,-10,-11
   dc.b -1,-2,-2,-3,-4,-5,-5,-6,-7,-8,-8,-9,-10,-11,-11,-12
   dc.b -1,-2,-2,-3,-4,-5,-6,-7,-7,-8,-9,-10,-11,-11,-12,-13
   dc.b -1,-2,-3,-4,-4,-5,-6,-7,-8,-9,-10,-11,-11,-12,-13,-14
   dc.b -1,-2,-3,-4,-5,-6,-7,-8,-8,-9,-10,-11,-12,-13,-14,-15
   dc.b -1,-2,-3,-4,-5,-6,-7,-8,-9,-10,-11,-12,-13,-14,-15,-16
  	even

  ;----------------------------------------------------------------------------


  .bss
  fadetablepos: ds.w 1

  fadecomponents: ; 288 bytes
;  	rept 16
;    	dc.l 0 ; address of fadetable
;    	dc.w 0 ; R
;    	dc.l 0 ; address of fadetable
;    	dc.w 0 ; G
;    	dc.l 0 ; address of fadetable
;    	dc.w 0 ; B
;  	endr
  ds.b 288
  .68000

endif  ; if PALETTE_FADING_ENABLE


;// Palette fading
;//////////////////////////////////////////////////////////////////////////////////////////////
;// Screen clearing and copying


clear1bplnoblaster:
; In: a0 - address to bitplane to clear
  mPush d0,d7,a0
  move.l #0,d0
  move.l #200-1,d7
  .oneline:
    move.w d0,(a0)
    offs set 8
    rept 19
      move.w d0,offs(a0)
      offs set offs+8
    endr
    add.l #160,a0
  dbra d7,.oneline
  mPop d0,d7,a0
  rts


;------------------------------------------------------------------------------


clear1bpl:
; In: a0 - address to clear, bitplane offset already added
	mPush a6
	move.l #$ffff8a00,a6 ;32 byte halftone ram
	rept 16/2
	  clr.l (a6)+
	endr
	move.w #0,$ffff8a20 ;source x inc
	move.w #0,$ffff8a22 ;source y inc
	move.l blanklongword,$ffff8a24 ;source address
	move.w #-1,$ffff8a28 ;endmask 1
	move.w #-1,$ffff8a2a ;endmask 2
	move.w #-1,$ffff8a2c ;endmask 3
	move.w #8,$ffff8a2e ;dest x inc
	move.w #8,$ffff8a30 ;dest y inc
	move.l a0,$ffff8a32 ;destination address
	move.w #20,$ffff8a36 ;x count (n words per line to copy)
	move.w #200,$ffff8a38 ;y count (n lines to copy)
	move.b #BLASTER_HOP_HALFTONE,$ffff8a3a ;blit hop (halftone mix)
	move.b #BLASTER_OP_ZEROES,$ffff8a3b ;blit op (logic op)
	move.b #0,$ffff8a3d ; skew
	move.b #BLASTER_COMMAND_START_HOG_MODE,$ffff8a3c ;blaster control, start blaster
	mPop a6
	rts


;------------------------------------------------------------------------------


copy32000noblaster:
; In: a0 - address of image
;     a1 - address of destination
	movem.l d0-d7,-(sp)
	move.l #80-1,d0
  .loop:
  	rept 100
  	  move.l (a0)+,(a1)+
  	endr
	dbra d0,.loop
	movem.l (sp)+,d0-d7
	rts


;------------------------------------------------------------------------------


clearsmokescreennoblaster:
	movem.l a0/d0-d1,-(sp)
	move.l smokescreenaddress,a0
	moveq.l #0,d1
	move.l #200-1,d0
  .loop:
  	rept 40
  	  move.l d1,(a0)+
  	endr
	dbra d0,.loop
	movem.l (sp)+,a0/d0-d1
	rts


;------------------------------------------------------------------------------


clearscreennoblaster:
	movem.l a0/d0-d1,-(sp)
;	move.l physscreen,a0
  move.l screenaddress,a0
	move.l #0,d1
	move.l #200-1,d0
  .loop:
  	rept 40
  	  move.l d1,(a0)+
  	endr
	dbra d0,.loop
	movem.l (sp)+,a0/d0-d1
	rts


;------------------------------------------------------------------------------


copy32000:
; In: a0 - source address
;     a1 - destination address
	mPush a6
	move.l #$ffff8a00,a6 ;32 byte halftone ram
	rept 16/2
	  clr.l (a6)+
	endr
	move.b #0,$ffff8a3d ; skew
	move.w #2,$ffff8a20 ;source x inc
	move.w #2,$ffff8a22 ;source y inc
	move.l a0,$ffff8a24 ;source address
	move.w #-1,$ffff8a28 ;endmask 1
	move.w #-1,$ffff8a2a ;endmask 2
	move.w #-1,$ffff8a2c ;endmask 3
	move.w #2,$ffff8a2e ;dest x inc
	move.w #2,$ffff8a30 ;dest y inc
	move.l a1,$ffff8a32 ;destination address
	move.w #80,$ffff8a36 ;x count (n words per line to copy)
	move.w #200,$ffff8a38 ;y count (n lines to copy)
	move.b #BLASTER_HOP_SOURCE,$ffff8a3a ;blit hop (halftone mix)
	move.b #BLASTER_OP_SOURCE,$ffff8a3b ;blit op (logic op)
	move.b #BLASTER_COMMAND_START_HOG_MODE,$ffff8a3c ;blaster control, start blaster
	mPop a6
	rts


;------------------------------------------------------------------------------


clear32000:
	mPush a6
	move.l #$ffff8a00,a6 ;32 byte halftone ram
	rept 16/2
	  clr.l (a6)+
	endr
	move.w #0,$ffff8a20 ;source x inc
	move.w #0,$ffff8a22 ;source y inc
	move.l blanklongword,$ffff8a24 ;source address
	move.w #-1,$ffff8a28 ;endmask 1
	move.w #-1,$ffff8a2a ;endmask 2
	move.w #-1,$ffff8a2c ;endmask 3
	move.w #2,$ffff8a2e ;dest x inc
	move.w #2,$ffff8a30 ;dest y inc
	move.l a0,$ffff8a32 ;destination address
	move.w #80,$ffff8a36 ;x count (n words per line to copy)
	move.w #200,$ffff8a38 ;y count (n lines to copy)
	move.b #BLASTER_HOP_HALFTONE,$ffff8a3a ;blit hop (halftone mix)
	move.b #BLASTER_OP_ZEROES,$ffff8a3b ;blit op (logic op)
	move.b #BLASTER_COMMAND_START_HOG_MODE,$ffff8a3c ;blaster control, start blaster
	mPop a6
	rts


;------------------------------------------------------------------------------


clearscreen:
	mPush a0
	move.l screenaddress,a0
	bsr clear32000
	mPop a0
	rts


;------------------------------------------------------------------------------


clearsmokescreen:
	mPush a0
	move.l smokescreenaddress,a0
	bsr clear32000
	mPop a0
	rts


;// Screen clearing and copying
;//////////////////////////////////////////////////////////////////////////////////////////////
;// Macro helpers


mSetPalette_code:
; In: a0 - point to address of palette
	movem.l a1/d0-d7,-(sp)
	movem.l (a0),d0-d7
	lea currentpalette,a1
	movem.l	d0-d7,(a1)
	movem.l (sp)+,a1/d0-d7
	rts


;------------------------------------------------------------------------------


mMarkCurrentScanline_code:
  mPush d7
  not.w $ffff8240.w
  move.l #40,d7
  .loopalittle:
  dbra d7,.loopalittle
  not.w $ffff8240.w
  mPop d7
  rts


;------------------------------------------------------------------------------


mSwapScreens_code:
  mPush a0
	move.l screenaddress,a0
	move.l smokescreenaddress,screenaddress
	move.l a0,smokescreenaddress
  mPop a0
	rts


;// Macro helpers
;//////////////////////////////////////////////////////////////////////////////////////////////
;// Other stuffs and thingamabobs


getrnd: ; random32
; Can't remember where I found this snippet
; In:  nothing
; Out: d0.l - pseudo-random number
  move.l  getrnd_seed(pc),d0
  add.l   D0,D0
  bcc.s   .done
  eori.b  #$AF,D0
.done:
  move.l  D0,getrnd_seed
  rts
getrnd_seed:
  dc.b "XiA!"


;------------------------------------------------------------------------------


resetblaster:
	mPush a0,d7
	move.l #$ffff8a00,a0
	move.l #$3d,d7
  .loop:
  	clr.b (a0)+
	dbra d7,.loop
	mPop a0,d7
	rts


;// Other stuffs and thingamabobs
;//////////////////////////////////////////////////////////////////////////////////////////////


;// Subroutines
;//
;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;//
;// Interrupt handlers


vbl:
	mPush d0,d1,d2,d3,d4,d5,d6,d7,a0

  ; Set palette
	lea currentpalette,a0
	movem.l (a0),d0-d7
	movem.l d0-d7,$ffff8240.w

  ; Set screen pointer
	move.l #$ffff8203,a0
	move.l screenaddress,d0
	movep.l d0,(0,a0)

	mPop d0,d1,d2,d3,d4,d5,d6,d7,a0

  ; Update counters
	move.w #$0,vblflag
	add.l #1,framecounter
	add.w #1,vblwaiter
	
  ; Music playing here
  if MUSIC_ENABLE
    tst.w ym_music_enable
    beq .nomusic
    jsr ym_music_vbl
    .nomusic:
  endif
	
 ; Vbl install thingie
	mPush a0
	move.l vblextra,a0
	jsr (a0)
	mPop a0

  ; Left shift pressed?
	cmp.b #$2a,$fffffc02
	beq exitall

	rte


;------------------------------------------------------------------------------


hbl:
timer_a:
timer_b:
timer_c:
timer_d:
acia:
  rte


;------------------------------------------------------------------------------


blank_vbl:
  rts


;// Interrupt handlers
;//
;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;//
;// Included codegroups


;//////////////////////////////////////////////////////////////////////////////////////////////
;// XA


if XA_ENABLE

  if XA_VERSION=7

    include "_inc\\mXA07.s"
    ; xa_set_params
        ; In:  d0.l - number of bitplanes in screen mode
        ;      a0.l - pointer to XA file
    ; xa_get_number_of_frames
        ; In:  a0.l - xa07 data
        ; Out: d0.l - number of frames
    ; xa_get_palette
        ; In:  a0.l - xa07 data
        ;      a1.l - palette destination
        ; Out: writes XA file's palette to (a1)
    ; xa_unpack_one_frame::
        ; In:  a0.l - xa07 data
        ;      a1.l - screen address
        ;      a2.l - background screen (UNUSED)

  endif  ; if XA_VERSION=7

endif  ; if XA_ENABLE


;// XA
;//////////////////////////////////////////////////////////////////////////////////////////////
;// Mallory: Atari ST/STE memory manager


if MALLORY_ENABLE

  include "_inc/mallory.s"
  ; mallory_init  ; the actual one-time allocation of memory
      ; In:  d0.l - Amount of RAM requested
      ; Out: d0.w - 0 if ok, otherwise error code
    
  ; mallory_gimme
      ; In:  d0.l - Amount of RAM requested
      ;      d1.w - 0 = no align, 1=word align, 2=long align
      ; Out: d0.w - 0 if ok, otherwise error code
      ;      d1.l - start of requested memory

  ; mallory_clear_all  ; actually only resets pointer, clears nothing

  ; mallory_exit  ; does jack shit, but makes it all look very very professional

endif

;// Mallory: Atari ST/STE memory manager
;//////////////////////////////////////////////////////////////////////////////////////////////


;// Included codegroups
;//
;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;//
;// DATA
.data


blanklongword:  dc.l 0  ; Now THIS is defensive programming! You'd think this could just as
                        ; well be in the BSS section, but just in case the BSS isn't actually
                        ; cleared when this program executes, we need to be absolutely sure
                        ; it's 0 at startup. This is how the BIG boys program!


;///////////////////////////////////////////////////////////////////////////////////////////////
;// Palettes


gem_pal:
		dc.w	$0777,$0700,$0070,$0770,$0007,$0707,$0077,$0555
		dc.w	$0333,$0733,$0373,$0773,$0337,$0737,$0377,$0000

allwhitepal:
	dc.w $0fff,$0fff,$0fff,$0fff,$0fff,$0fff,$0fff,$0fff
	dc.w $0fff,$0fff,$0fff,$0fff,$0fff,$0fff,$0fff,$0fff

allblackpal:
	dc.w 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0

bwpal:
	dc.w	$0000,$0888,$0111,$0999,$0222,$0AAA,$0333,$0BBB
	dc.w	$0444,$0CCC,$0555,$0DDD,$0666,$0EEE,$0777,$0FFF

systemtest_palette:
  dc.w $180
  dcb.w 15,$ffd

testpal:
  dc.w 0
  dcb.w 15,$fff

butterfly_pal:
  dc.w $344+$800, $012
  dcb.w 14,$f00


;// Palettes
;///////////////////////////////////////////////////////////////////////////////////////////////


;// DATA
;//
;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;//
;// BSS
.bss


vblflag:        ds.w 1
framecounter:   ds.l 1
vblwaiter:      ds.w 1

screenaddress:        ds.l 1
smokescreenaddress:   ds.l 1
	ds.b 256
smokescreen1:         ds.b 32000
smokescreen2:         ds.b 32000

currentpalette:       ds.w 16


;//////////////////////////////////////////////////////////////////////////////////////////////
;// Restore stuff for exiting cleanly


oldrez:  ds.w 1
oldvbl:  ds.l	1
old114:  ds.l	1
;oldusp:	ds.l	1
oldpal:  ds.w 16
old8260: ds.l 1
old820a: ds.l 1
old8265: ds.b 1
old820f: ds.b 1
old8e21: ds.b 1
	even

physbase: ds.l 1
old8203:  ds.b 1
old8201:  ds.b 1
old820d:  ds.b 1
old484:   ds.b 1
	even

startupsp: ds.l 1

; From DHS' demosystem
save_hbl:		  ds.l  1			; HBL vector
save_vbl:		  ds.l  1			; VBL vector
save_timer_a: ds.l  1			; Timer A vector
save_timer_b: ds.l  1			; Timer B vector
save_timer_c: ds.l  1			; Timer C vector
save_timer_d: ds.l  1			; Timer D vector
save_acia:    ds.l  1			; ACIA vector
save_usp:     ds.l  1			; USP
save_mfp:     ds.b 16     ; MFP


;// Restore stuff for exiting cleanly
;//////////////////////////////////////////////////////////////////////////////////////////////


;// BSS
;//
;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////
;//////////////////////////////////////////////////////////////////////////////////////////////



