@echo off
rem ####################################################################
rem ## Make script
rem ##
rem ## Copy makefile to temporary file, changing spaces to tabs
fixmake.py Makefile_Programs Makefile_Programs.fixed
fixmake.py Makefile Makefile.fixed
rem ## Remove old object file, to force make to rebuild it every time
rm -f main.tos
rem ## ...and finally, make the project
make %1 -f Makefile.fixed
rem ##
rem ## Make script
rem ####################################################################
