## blst_spr - Blaster sprites for Atari STe/TT/Falcon

Code used in [Beyond Brown tutorial on blaster sprites](http://http://beyondbrown.mooo.com//post/blaster-sprites/ "Beyond Brown tutorial on blaster sprites")

This code is STE only, but the sprite routines themselves
("blsp_draw_noclip" and "blsp_draw_fullclip") should work on any
16/32-bit Atari machine with a blaster chip.

To assemble, run "rmac -p -v main.s -o main.tos". I'm only
supplying rmac for Windows, so if you're on a different platform,
you'll need to get and build rmac for your platform.

You won't be able to build using GNU Make on any machine but mine
(because lots and lots of proprietary converter scripts). However,
as long as you only edit main.s, this should work fine.

**Note: At the time of writing, X clipping where fewer than the 16
rightmost pixels of a sprite are being shown does not display
correctly in neither Hatari or STEem. It does work on a real STE,
which is all that matters.**